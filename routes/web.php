<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
// Controladores Propios
use App\Http\Controllers\PdfController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\PromoterController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\ProjectTaskController;
use App\Http\Controllers\DepartmentTaskController;


Route::get('/', HomeController::class)->name('home')
    ->middleware('auth');

// Conjunto de Rutas para PROYECTOS
Route::group(
    ['prefix' => 'proyectos',       // Todas tendrán un prefijo común
    'middleware' => 'auth'],        // Utilizarán el middleware 'auth'
    function(){
        Route::get('', [ProjectController::class, 'index'])->name('projects.index');
        Route::post('', [ProjectController::class, 'store'])->name('projects.store')
            ->middleware('isJefe');
        Route::get('crear', [ProjectController::class, 'create'])->name('projects.create')
            ->middleware('isJefe');
        Route::get('{project}', [ProjectController::class, 'show'])->name('projects.show');
        Route::put('{project}', [ProjectController::class, 'update'])->name('projects.update')
            ->middleware('isResponsable');
        Route::get('{project}/editar', [ProjectController::class, 'edit'])->name('projects.edit')
            ->middleware('isResponsable');
        // Conjunto de Rutas para detalle de TAREAS POR PROYECTOS
        Route::group(
            ['prefix' => '{project}/tareas'],     // Todas tendrán un prefijo común
            function(){
                Route::post('', [ProjectTaskController::class, 'store'])->name('projectsTasks.store');
                Route::get('crear', [ProjectTaskController::class, 'create'])->name('projectsTasks.create');
                Route::get('{task}', [ProjectTaskController::class, 'show'])->name('projectsTasks.show');
                Route::put('{task}', [ProjectTaskController::class, 'update'])->name('projectsTasks.update');
                Route::delete('{task}', [ProjectTaskController::class, 'destroy'])->name('projectsTasks.destroy');
                Route::get('{task}/editar', [ProjectTaskController::class, 'edit'])->name('projectsTasks.edit');
        });
});

// Conjunto de Rutas para PROMOTORES
Route::group(
    ['prefix' => 'promotores',      // Todas tendrán un prefijo común
    'middleware' => 'auth'],        // Utilizarán el middleware 'auth'
    function(){
        Route::get('', [PromoterController::class, 'index'])->name('promoters.index');
        Route::post('', [PromoterController::class, 'store'])->name('promoters.store');
        Route::get('crear', [PromoterController::class, 'create'])->name('promoters.create');
        Route::get('{promoter}', [PromoterController::class, 'show'])->name('promoters.show');
        Route::put('{promoter}', [PromoterController::class, 'update'])->name('promoters.update');
        Route::get('{promoter}/editar', [PromoterController::class, 'edit'])->name('promoters.edit');
});

// Conjunto de Rutas para DEPARTAMENTOS
Route::group(
    ['prefix' => 'departamentos',               // Todas tendrán un prefijo común
    'middleware' => ['auth','isAdmin']],        // Utilizarán el middleware 'auth'
    function(){
        Route::get('', [DepartmentController::class, 'index'])->name('departments.index');
        Route::post('', [DepartmentController::class, 'store'])->name('departments.store');
        Route::get('crear', [DepartmentController::class, 'create'])->name('departments.create');
        Route::get('{department}', [DepartmentController::class, 'show'])->name('departments.show');
        Route::put('{department}', [DepartmentController::class, 'update'])->name('departments.update');
        Route::get('{department}/editar', [DepartmentController::class, 'edit'])->name('departments.edit');
        Route::group(
            ['prefix' => '{department}/tarea'],
            function(){
                Route::post('', [DepartmentTaskController::class, 'store'])->name('departmentTask.store');
                Route::delete('{task}', [DepartmentTaskController::class, 'destroy'])->name('departmentTask.destroy');
            }
        );
});

//Rutas para USUARIOS
Route::group(
    ['prefix' => 'usuarios',
    'middleware' => ['auth','isAdmin']],
    function(){
        Route::get('', [UserController::class, 'index'])->name('users.index');
        Route::post('', [UserController::class, 'store'])->name('users.store');
        Route::get('crear', [UserController::class, 'create'])->name('users.create');
        Route::get('{user}', [UserController::class, 'show'])->name('users.show');
        Route::put('{user}', [UserController::class, 'update'])->name('users.update');
        Route::delete('{user}', [UserController::class, 'destroy'])->name('users.destroy');
        Route::get('{user}/editar', [UserController::class, 'edit'])->name('users.edit');
        Route::put('pass/{user}', [UserController::class, 'updatePass'])->name('usersPass.update');
        Route::get('pass/{user}/editar', [UserController::class, 'editPass'])->name('usersPass.edit');
});

// Conjunto de Rutas para TAREAS
Route::group(
    ['prefix' => 'tareas',                      // Todas tendrán un prefijo común
    'middleware' => ['auth','isAdmin']],        // Utilizarán el middleware 'auth'
    function(){
        Route::get('', [TaskController::class, 'index'])->name('tasks.index');
        Route::post('', [TaskController::class, 'store'])->name('tasks.store');
        Route::get('crear', [TaskController::class, 'create'])->name('tasks.create');
        Route::get('{task}', [TaskController::class, 'show'])->name('tasks.show');
        Route::put('{task}', [TaskController::class, 'update'])->name('tasks.update');
        Route::get('{task}/editar', [TaskController::class, 'edit'])->name('tasks.edit');
});

// Informes en PDF
Route::group(
    ['prefix' => 'pdf',
    'middleware' => 'auth'],
    function(){
        Route::get('{project}', PdfController::class)->name('pdf.dataProject');
});

// Conjunto de rutas para el LOGIN (incluidas en Laravel)
Auth::routes([
    'register' => false,    // Desactivamos la opción de registro
    'reset' => false,       // Desactivamos la opción de resetear contraseña para los usuarios
    'verify' => false,      // Desactivamos la necesidad de verificar contraseña
]);
