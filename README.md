## Acerca de ProyectosICT

ProyectosICT se trata de una aplicación web para el registro, control y seguimiento de los proyectos de Infraestructura Común de Telecomunicaciones (ICT) en cartera dentro un estudio de Arquitectura o Ingeniería de Telecomunicaciones en el ámbito de la Edificación. Además, será capaz de realizar un predimensionado de la infraestructura según el R.D. 346/2011, para facilitar las labores de cálculo de los Técnicos, y asignar trabajos y tareas a diferentes miembros del equipo redactor.

La aplicación se despliega mediante tecnologías web, y ha sido desarrollado mediante el framework Laravel.

## En desarrollo...

Actualmente me encuentro en desarrollo del proyecto. Pronto tendrás novedades...

## Más información

<p align="left"><a href="https://www.afvweb.es" target="_blank">https://www.afvweb.es</a></p>