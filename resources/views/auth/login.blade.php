@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card border-primary">
                <div class="card-header py-4 px-5 bg-primary text-light">
                    <h5 class="card-title">{{ config('app.name') }}</h5>
                    <p class="card-text">Programa para el registro, control y predimensionado de Proyectos de I.C.T. <small>(según R.D. 346/2011)</small></p>
                    <img src="img/logo.png" class="border rounded" alt="proyectosICT">
                </div>                
                <div class="card-header bg-info text-center">
                    <h5 class="mb-0 p-2">Inicio de Sesión</h5> 
                </div>
                <div class="card-body bg-light">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary"><i class="fa-solid fa-key me-2"></i>Iniciar Sesión</button>
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-footer bg-dark text-muted text-center">
                    <div class="container">
                        <div>{{ config('app.name') }} | Alejandro Fuentes Villar. {{date('Y')}}.</div>
                        <span class="me-2">Para más información puedes consultar el repositorio del proyecto en</span>
                        <a class="link-light text-decoration-none" href="https://gitlab.com/a.fuentesvillar/proyectosict"><i class="fa-brands fa-gitlab"></i> GitLab</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
