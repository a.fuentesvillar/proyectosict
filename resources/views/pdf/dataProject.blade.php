<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$project->code}} | Resumen</title>

        <!-- Implementamos las fuentes que vamos a utilizar en nuestra aplicación -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

        <link rel="stylesheet" href="/css/app.css">
        <!-- Estilos personalizados para el informe en PDF -->
        <style>
            .etiqueta{
                font-size: 0.875em;
                font-style: italic;
            }
            .linea{
                margin-top: 0.25rem;
                margin-bottom: 0.25rem;
            }
            header{
                padding-bottom: 1rem;
                border-bottom: solid;
            }
            h3{
                margin-bottom: 2rem;
            }
            fieldset{
                margin-bottom: 2rem;
            }
            footer{
                width: 100%;
                text-align: right;
                border-top: solid;
                position: fixed;
                bottom: 0;     
            }
        </style>
</head>
<body style="font-family: 'Nunito', sans-serif;">
    <header>
        <img src="img/logo.png" alt="{{config('app.name')}}">
    </header>
    <h3>Hoja de proyecto {{$project->code}}</h3>
    <fieldset>
        <legend>Datos Generales</legend>
        <div class="linea">
            <span class="etiqueta">Código: </span>
            <span>{{$project->code}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Nombre: </span>
            <span>{{$project->name}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Descripción:</span>
            <span>{{$project->description}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Localidad: </span>
            <span>{{$project->city}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Promotor: </span>
            <span>{{$project->promoter->name}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Fecha de encargo: </span>
            <span>{{$project->orderDate}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Encargado: </span>
            <span>{{$user->name}} {{$user->surname}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Estado: </span>
            <span>{{$project->state->name}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Nº PAU Totales: </span>
            <span>{{$project->nPau}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Tipo de Edificio: </span>
            <span>
                @if($project->tipoEdif === 'B')
                Viviendas en bloque
                @else
                Unifamiliares
                @endif
            </span>
        </div>
        <div class="linea">
            <span class="etiqueta">Nº PAU Máx. por Vertical: </span>
            <span>{{$project->numMaxPauVertical}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Nº PAU Máx. por Planta: </span>
            <span>{{$project->numMaxPauPlanta}}</span>
        </div>
    </fieldset>
    <fieldset>
        <legend>Dimensionado de la ICT:</legend>
        <div class="linea">
            <span class="etiqueta">Arqueta Exterior: </span>
            <span>{{$project->arqExterior}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Canalización Exterior: </span>
            <span>{{$project->canalExterior}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Registro de Enlace: </span>
            <span>{{$project->regEnlace}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Canalización de Enlace: </span>
            <span>{{$project->canalEnlace}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Recintos de Telecomunicaciones: </span>
            <span>{{$project->recintos}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Canalización Principal: </span>
            <span>{{$project->canalPrincipal}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Registro Secundario: </span>
            <span>{{$project->regSecundario}}</span>
        </div>
        <div class="linea">
            <span class="etiqueta">Canalización Secundaria: </span>
            <span>{{$project->canalSecundaria}}</span>
        </div>
    </fieldset>
    <footer>
        <small>Fecha de Impresión del documento: {{$date->format('d / m / Y')}}</small> 
    </footer>
</body>
</html>