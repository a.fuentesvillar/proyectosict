<fieldset class="row mb-3">
    <legend>Datos de la Tarea:</legend>
    <input type="hidden" name="project_id" value="{{$project->id}}">
    <input type="hidden" name="task_id" value="{{$task->id}}">
    <div class="col-md-2 mb-3">
        <label for="project_id" class="form-label">Proyecto:</label>
        <input type="text" class="form-control" value="{{$project->code}}" disabled>
    </div>
    <div class="col-md-5 mb-3">
        <label for="task_id" class="form-label">Tarea:</label>
        <input type="text" class="form-control" value="{{$task->name}}" disabled>
    </div>
    <div class="col-md-4 mb-3">
        <label for="user_id" class="form-label">Usuario:</label>
        <select name="user_id" id="user_id" class="form-select">
            <option value="">Seleccione el usuario...</option>
            @foreach ($users as $user)
            <option value="{{$user->id}}"
            @if ($user->id == old('user_id', $projectTask->user_id)) selected="selected" @endif
            >{{$user->dni}} | {{$user->name}} {{$user->surname}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-2 mb-3">
        <label for="asigDate" class="form-label">Fecha de Asignación:</label>
        <input type="date" class="form-control" name="asigDate" placeholder="Horas" value="{{old('asigDate', $projectTask->asigDate)}}">
    </div>
    <div class="col-md-2 mb-3">
        <label for="estimatedHours" class="form-label">Horas Estimadas:</label>
        <input type="number" class="form-control" name="estimatedHours" placeholder="Horas" value="{{old('estimatedHours', $projectTask->estimatedHours)}}">
    </div>
</fieldset>
<fieldset class="row mb-3">
    <legend>Ejecución de la Tarea:</legend>
    <div class="col-md-4 mb-3">
        <label for="state_id" class="form-label">Estado:</label>
        <select name="state_id" id="state_id" class="form-select">
            @foreach ($states as $state)
            <option value="{{$state->id}}"
            @if ($state->id == old('state_id', $projectTask->user_id)) selected="selected" @endif
            >{{$state->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-2 mb-3">
        <label for="doneDate" class="form-label">Fecha de Realización:</label>
        <input type="date" class="form-control" name="doneDate" placeholder="Horas" value="{{old('doneDate', $projectTask->doneDate)}}">
    </div>
    <div class="col-md-2 mb-3">
        <label for="realHours" class="form-label">Horas Reales:</label>
        <input type="number" class="form-control" name="realHours" placeholder="Horas" value="{{old('realHours', $projectTask->realHours)}}">
    </div>
    <div class="col-md-10 mb-3">
        <label for="description" class="form-label">Observaciones:</label>
        <textarea name="description" rows="3" class="form-control">{{old('description', $projectTask->description)}}</textarea>
    </div>
</fieldset>


