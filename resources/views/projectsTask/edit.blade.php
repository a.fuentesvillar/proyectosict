<!-- Plantilla de la que extiende la vista -->
@extends('layouts.app')
<!-- Titulo de la pestaña en la vista -->
@section('title', 'Editar Tarea')
<!-- Sección del título de la vista -->
@section('header')
  <!-- Título -->
  <h4 class="mb-0"><i class="fa-solid fa-clipboard-check me-2"></i>Editar Tarea Asignada</h4>
  <!-- Información sobre el camino de dónde nos encontramos dentro de la aplicación -->
  <nav class="navbar-nav ms-auto d-none d-md-block" aria-label="breadcrumb">
    <ol class="breadcrumb mb-0">
      <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
      <li class="breadcrumb-item"><a href="{{route('projects.index')}}">Proyectos</a></li>
      <li class="breadcrumb-item"><a href="{{route('projects.show', $project)}}">{{$project->code}}</a></li>
      <li class="breadcrumb-item active" aria-current="page">Editar Tarea</li>
    </ol>
  </nav>
@endsection
<!-- Sección del contenido de la vista -->
@section('content')
<div class="container">
    <!-- Advertencia sobre zona de edición -->
    <div class="bg-warning rounded p-3 mb-2">
        <h5 class="mb-0">Zona de Edición de registros</h5>
    </div>
    <!-- Formulario para crear los registros -->
    <form class="row" method="POST" action="{{route('projectsTasks.update', [$project,$task])}}", enctype="multipart/form-data">
        @csrf
        @method('put')
        <!-- Se crea una plantilla parcial con los inputs de los formularios para no tener que duplicarlos -->
        @include('projectsTask._form')
        <div class="d-flex justify-content-center m-2">
        <button type="submit" class="btn btn-warning me-2"><i class="fa-solid fa-pencil me-2"></i>Editar</button>
        <a href="{{route('projects.show', [$project,$task])}}" class="btn btn-secondary"><i class="fa-solid fa-xmark me-2"></i>Cancelar</a>
        </div>
    </form>
</div>
@endsection