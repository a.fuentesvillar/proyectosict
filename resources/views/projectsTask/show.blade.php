<!-- Plantilla de la que extiende la vista -->
@extends('layouts.app')
<!-- Titulo de la pestaña en la vista -->
@section('title', 'Ver Tarea')
<!-- Sección del título de la vista -->
@section('header')
  <!-- Título -->
  <h4 class="mb-0"><i class="fa-solid fa-clipboard-check me-2"></i>Ver Tarea Asignada</h4>
  <!-- Información sobre el camino de dónde nos encontramos dentro de la aplicación -->
  <nav class="navbar-nav ms-auto d-none d-md-block" aria-label="breadcrumb">
    <ol class="breadcrumb mb-0">
      <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
      <li class="breadcrumb-item"><a href="{{route('projects.index')}}">Proyectos</a></li>
      <li class="breadcrumb-item"><a href="{{route('projects.show', $project)}}">{{$project->code}}</a></li>
      <li class="breadcrumb-item active" aria-current="page">Ver Tarea</li>
    </ol>
  </nav>
@endsection
<!-- Sección del contenido de la vista -->
@section('content')
<div class="container mb-3">
    @include('projectsTask._formShow')
    <div class="d-flex justify-content-center">
        <a class="btn btn-primary me-2" href="{{route('projects.edit', $project)}}" ><i class="fa-solid fa-arrow-rotate-left me-2"></i>Volver</a>
    </div>
</div>
@endsection