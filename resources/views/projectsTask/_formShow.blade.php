<fieldset class="row mb-3" disabled>
    <legend>Datos de la Tarea:</legend>
    <div class="col-md-2 mb-3">
        <label for="project_id" class="form-label">Proyecto:</label>
        <input type="text" class="form-control" value="{{$projectTask->project->code}}">
    </div>
    <div class="col-md-5 mb-3">
        <label for="task_id" class="form-label">Tarea:</label>
        <input type="text" class="form-control" value="{{$projectTask->task->name}}">
    </div>
    <div class="col-md-4 mb-3">
        <label for="user_id" class="form-label">Usuario:</label>
        <input type="text" class="form-control" value="{{$projectTask->user->name}}">
    </div>
    <div class="col-md-2 mb-3">
        <label for="asigDate" class="form-label">Fecha de Asignación:</label>
        <input type="date" class="form-control" value="{{$projectTask->asigDate}}">
    </div>
    <div class="col-md-2 mb-3">
        <label for="estimatedHours" class="form-label">Horas Estimadas:</label>
        <input type="number" class="form-control" value="{{$projectTask->estimatedHours}}">
    </div>
</fieldset>
<fieldset class="row mb-3" disabled>
    <legend>Ejecución de la Tarea:</legend>
    <div class="col-md-4 mb-3">
        <label for="state_id" class="form-label">Estado:</label>
        <input type="text" class="form-control" value="{{$projectTask->state->name}}">
    </div>
    <div class="col-md-2 mb-3">
        <label for="doneDate" class="form-label">Fecha de Realización:</label>
        <input type="text" class="form-control" value="{{$projectTask->doneDate}}">
    </div>
    <div class="col-md-2 mb-3">
        <label for="realHours" class="form-label">Horas Reales:</label>
        <input type="number" class="form-control" value="{{$projectTask->realHours}}">
    </div>
    <div class="col-md-10 mb-3">
        <label for="description" class="form-label">Observaciones:</label>
        <textarea rows="3" class="form-control">{{$projectTask->description}}</textarea>
    </div>
</fieldset>


