<!-- Plantilla de la que extiende la vista -->
@extends('layouts.app')
<!-- Titulo de la pestaña en la vista -->
@section('title', 'Usuario')
<!-- Sección del título de la vista -->
@section('header')
<!-- Título -->
    <h4 class="mb-0"><i class="fa-solid fa-users me-2"></i>{{$user->name}}</h4>
    <!-- Información sobre el camino de dónde nos encontramos dentro de la aplicación -->
    <nav class="navbar-nav ms-auto d-none d-md-block" aria-label="breadcrumb">
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{route('users.index')}}">Usuarios</a></li>
            <li class="breadcrumb-item active" aria-current="page">Datos del Usuario</li>
        </ol>
    </nav>
@endsection
<!-- Sección del contenido de la vista -->
@section('content')
    <div class="container mb-3">
        <div class="row">
            <div class="col-md-3 mb-3">
                <label for="dni" class="form-label">D.N.I.:</label>
                <input type="text" class="form-control" name="dni" value="{{$user->dni}}" disabled>
            </div>
            <div class="col-md-4 mb-3">
                <label for="name" class="form-label">Nombre:</label>
                <input type="text" class="form-control" name="name" value="{{$user->name}}" disabled>
            </div>
            <div class="col-md-5 mb-3">
                <label for="surname" class="form-label">Apellidos:</label>
                <input type="text" class="form-control" name="surname" value="{{$user->surname}}" disabled>
            </div>
            <div class="col-md-6 mb-3">
                <label for="email" class="form-label">Correo:</label>
                <input type="email" class="form-control" name="email" value="{{$user->email}}" disabled>
            </div>
            <div class="col-md-6 mb-3">
                <label for="department_id" class="form-label">Departamento:</label>
                <input type="text" class="form-control" name="department_id" value="{{$user->department_id}} | {{$user->department->name}}" disabled>
            </div>
            <div class="col-md-6 mb-3">
                <label for="created_at" class="form-label">Fecha de Creación:</label>
                <input type="text" class="form-control" name="created_at" value="{{$user->created_at->diffForHumans()}}" disabled>
            </div>
            <div class="col-md-6 mb-3">
                <label for="updated_at" class="form-label">Última Modificación:</label>
                <input type="text" class="form-control" name="updated_at" value="{{$user->updated_at->diffForHumans()}}" disabled>
            </div>
        </div>
    </div>
    <!-- Pie de la sección -->
    <div class="d-flex justify-content-center">
        <a class="btn btn-primary me-2" href="{{route('users.index')}}"><i class="fa-solid fa-arrow-rotate-left me-2"></i>Volver</a>
        <a class="btn btn-warning" href="{{route('users.edit', $user)}}"><i class="fa-solid fa-pencil me-2"></i>Editar</a>
    </div>
@endsection