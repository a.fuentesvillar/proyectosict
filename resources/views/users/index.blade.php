<!-- Plantilla de la que extiende la vista -->
@extends('layouts.app')
<!-- Titulo de la pestaña en la vista -->
@section('title', 'Usuarios')
<!-- Sección del título de la vista -->
@section('header')
<!-- Título -->
<h4 class="mb-0"><i class="fa-solid fa-users me-2"></i>Usuarios</h4>
<!-- Información sobre el camino de dónde nos encontramos dentro de la aplicación -->
<nav class="navbar-nav ms-auto" aria-label="breadcrumb">
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
        <li class="breadcrumb-item active" aria-current="page">Usuarios</li>
    </ol>
</nav>
@endsection
<!-- Sección del contenido de la vista -->
@section('content')
<!-- Sección para el botón de nuevo registro -->
<div class="d-flex justify-content-end mb-2">
    <a class="btn btn-success" href="{{route('users.create')}}"><i class="fa-solid fa-plus me-2"></i>Nuevo</a>
</div>
<table class="table table-hover align-middle">
    <thead class="table-dark">
        <tr class="row">
            <th class="col">Nombre</th>
            <th class="col-2 d-xxl-block d-none">Apellidos</th>
            <th class="col-2 d-xxl-block d-none">DNI</th>
            <th class="col-2 d-xxl-block d-none">Correo</th>
            <th class="col-2">Departamento</th>
            <th class="col text-end">Opciones</th>
        </tr>
    </thead>
    <tbody>
        <!-- Recorremos todos los registros pasados a la vista mediante el modelo para acceder a ellos -->
        @foreach ($users as $user)
        <tr class="row">
            <td class="col">{{$user->name}}</td>
            <td class="col-2 d-xxl-block d-none">{{$user->surname}}</td>
            <td class="col-2 d-xxl-block d-none">{{$user->dni}}</td>
            <td class="col-2 d-xxl-block d-none">{{$user->email}}</td>
            <th class="col-2 d-xxl-block d-none">{{$user->department->name}}</th>
            <td class="col text-end">
                <!-- Creamos el botón para ver los detalles del registro -->
                <a class="btn btn-primary" href="{{route('users.show', $user)}}"><i class="fa-solid fa-eye"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<!-- Sección para el listado de páginas de registros -->
<div class="d-flex justify-content-center m-2">
    {!!$users->links()!!}
</div>
@endsection
