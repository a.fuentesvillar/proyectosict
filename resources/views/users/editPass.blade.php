<!-- Plantilla de la que extiende la vista -->
@extends('layouts.app')
<!-- Titulo de la pestaña en la vista -->
@section('title', 'Editar Contraseña de Usuario')
<!-- Sección del título de la vista -->
@section('header')
<!-- Título -->
<h4 class="mb-0"><i class="fa-solid fa-users me-2"></i>Editar {{$user->name}}</h4>
    <!-- Información sobre el camino de dónde nos encontramos dentro de la aplicación -->
    <nav class="navbar-nav ms-auto d-none d-md-block" aria-label="breadcrumb">
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{route('users.index')}}">Usuarios</a></li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{route('users.show', $user)}}">{{$user->name}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Editar Contraseña</li>
        </ol>
    </nav>
@endsection
<!-- Sección del contenido de la vista -->
@section('content')
<div class="container">
    <!-- Advertencia sobre zona de edición -->
    <div class="bg-danger text-light rounded p-3 mb-2">
        <h5 class="mb-0"><i class="fa-solid fa-triangle-exclamation me-2"></i>Zona de cambio de contraseña</h5>
    </div>
    <!-- Formulario para editar los registros -->
    <form class="row" method="POST" action="{{route('usersPass.update', $user)}}", enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="col-md-4 mb-3">
            <label for="password" class="form-label">Contraseña:</label>
            <input type="password" class="form-control" name="password">
        </div>
        <div class="col-md-4 mb-3">
            <label for="password_confirmation" class="form-label">Repetir Contraseña:</label>
            <input type="password" class="form-control" name="password_confirmation">
        </div>
        <div class="d-flex justify-content-center m-2">
            <button type="submit" class="btn btn-danger me-2"><i class="fa-solid fa-key me-2"></i>Actualizar Contraseña</button>
            <a href="{{route('users.index')}}" class="btn btn-secondary me-5"><i class="fa-solid fa-xmark me-2"></i>Cancelar</a>
        </div>
    </form>
</div>
@endsection