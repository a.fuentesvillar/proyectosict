<div class="col-md-3 mb-3">
    <label for="dni" class="form-label">D.N.I.:</label>
    <input type="text" class="form-control" name="dni" value="{{old('dni', $user->dni)}}" autofocus>
</div>
<div class="col-md-4 mb-3">
    <label for="name" class="form-label">Nombre:</label>
    <input type="text" class="form-control" name="name" value="{{old('name', $user->name)}}">
</div>
<div class="col-md-5 mb-3">
    <label for="surname" class="form-label">Apellidos:</label>
    <input type="text" class="form-control" name="surname" value="{{old('surname', $user->surname)}}">
</div>
<div class="col-md-6 mb-3">
    <label for="email" class="form-label">Correo:</label>
    <input type="email" class="form-control" name="email" value="{{old('email', $user->email)}}" disabled>
</div>
<div class="col-md-6 mb-3 @if($user->id === 1) d-none @endif">
    <label for="department_id" class="form-label">Departamento:</label>
    <select name="department_id" id="department_id" class="form-select">
        @foreach ($departments as $department)
        <option value="{{$department->id}}"
            @if ($department->id == old('department_id', $user->department_id)) selected="selected" @endif
            >{{$department->id}} | {{$department->name}}</option>
        @endforeach
    </select>
</div>


