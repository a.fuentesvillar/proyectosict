<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- 
        CSRF Token Laravel.
        Laravel implementa un token CSRF, que lo que hace es comprobar solicitudes y peticiones para ataques
     -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
     <!-- 
        Se crea una nueva sección para poder editar el título de la pestaña en cada vista.
        También se incluye el nombre de la aplicación mediante config('app.name')
      -->
    <title>
        @yield("title") | {{ config('app.name') }}
    </title>
    <link rel="shortcut icon" type="/image/x-icon" href="img/favicon.png" />
    <!-- Implementamos Fontawesome 6 para disponer de multitud de iconos gratuitos en la aplicación -->
    <script src="/js/fontawesome6.all.min.js" defer></script>
    <!-- Implementamos las fuentes que vamos a utilizar en nuestra aplicación -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    <!-- Scripts por defecto de laravel. Utilizamos el motor Vite incluido en laravel -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body class="bg-white">
    <div id="app">
        <!-- Se crea una plantilla parcial sólamente para el menú de navegación -->
        @auth
        @include('layouts.partials.navbar')
        @endauth
        <main>
            @auth
            <!-- Se crea una nueva sección dentro de la plantilla para ubicar el título y la ruta en cada una de las vistas -->
            <div class="bg-light border py-3 px-xl-3">
                <div class="container d-flex justify-content-between">
                    @yield('header')
                </div>
            </div>
            @endauth
            <div class="container">
                <div class="mt-3">
                    <!-- Se crea una plantilla parcial para los mensajes de error que puedan tener las diferentes vistas -->
                    @include('layouts.partials._errors')
                    <!-- Se crea una plantilla parcial para los mensajes de sesión que puedan tener las diferentes vistas -->
                    @include('layouts.partials._sessions')
                </div>
                <!-- Se crea una nueva sección para todo el contenido que muestra cada vista -->
                <div class="p-xl-3 mb-5 bg-white">
                    @yield('content')
                </div>
            </div>
        </main>
    </div>
</body>
</html>
