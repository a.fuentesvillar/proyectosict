<nav class="navbar navbar-expand-md navbar-dark bg-primary py-2">
    <div class="container">
        <span class="navbar-brand">
            <img src="/img/logo.png" class="border rounded" alt="proyectosICT">
        </span>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Iconos a la Izquierda de la barra de navegación -->
            <ul class="navbar-nav me-auto">
                <li class="navbar-item"><a class="nav-link" href="{{route('home')}}"><i class="fa-solid fa-house me-1"></i>Inicio</a></li>
                <li class="navbar-item"><a class="nav-link" href="{{route('projects.index')}}"><i class="fas fa-pencil-ruler me-1"></i>Proyectos</a></li>
                <li class="navbar-item"><a class="nav-link" href="{{route('promoters.index')}}"><i class="fa-solid fa-helmet-safety me-1"></i>Promotores</a></li>
                @if(Auth::user()->isAdmin())
                <div class="dropdown">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fa-solid fa-gear me-1"></i>Configuración
                            </a>
                            <ul class="dropdown-menu dropdown-menu-dark bg-primary">
                                <li class="dropdown-item" href="#"><a class="nav-link" href="{{route('departments.index')}}"><i class="fa-solid fa-building-user me-1"></i>Departamentos</a></li>
                                <li class="dropdown-item" href="#"><a class="nav-link" href="{{route('users.index')}}"><i class="fa-solid fa-users me-1"></i>Usuarios</a></li>
                                <li class="dropdown-item" href="#"><a class="nav-link" href="{{route('tasks.index')}}"><i class="fa-solid fa-clipboard-check me-1"></i>Tareas</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                @endif
            </ul>
            <!-- Iconos a la Derecha de la barra de navegación -->
            <ul class="navbar-nav ms-auto">
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="btn btn-outline-light dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <i class="fa-solid fa-user-tie me-2"></i>
                        {{ Auth::user()->name }} 
                        <small>({{ Auth::user()->department->name }})</small>
                    </a>
                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>