<!-- 
    Plantilla creada para los mensajes de información de sesión en las diferentes vistas.
    Estos mensajes proceden de los diferentes métodos de los diferentes controladores de los Modelos.
    Se inserta en el fichero app.blade.php.
-->
@if (session('status')) <!-- Si existe un mensaje de status en la sesión, lo muestra como un alert de bootstrap en la pantalla -->
    <div class="alert alert-success" role="alert">
        <i class="fas fa-check-circle me-2"></i>{{session('status')}}
    </div>
@endif