<!-- 
    Plantilla creada para los mensajes de información de sesión en las diferentes vistas.
    Se inserta en el fichero app.blade.php.
-->
@if ($errors->any()) <!-- Si existe algún error, crea una lista desordenada con cada uno de los mensajes. -->
    <div class="alert alert-danger" role="alert">
        <ul class="mb-0">
        @foreach ($errors->all() as $error)
        <li class="list-group-item list-group-item-danger"><i class="fa-solid fa-circle-xmark me-2"></i>{{$error}}</li>
        @endforeach
        </ul>
    </div>
@endif