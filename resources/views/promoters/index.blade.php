<!-- Plantilla de la que extiende la vista -->
@extends('layouts.app')
<!-- Titulo de la pestaña en la vista -->
@section('title', 'Promotores')
<!-- Sección del título de la vista -->
@section('header')
    <!-- Título -->
    <h4 class="mb-0"><i class="fa-solid fa-helmet-safety me-2"></i>Promotores</h4>
    <!-- Información sobre el camino de dónde nos encontramos dentro de la aplicación -->
    <nav class="navbar-nav ms-auto" aria-label="breadcrumb">
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Promotores</li>
        </ol>
    </nav>
@endsection
<!-- Sección del contenido de la vista -->
@section('content')
    <!-- Sección para el botón de nuevo registro -->
    <div class="d-flex justify-content-end mb-2">
        <a class="btn btn-success" href="{{route('promoters.create')}}"><i class="fa-solid fa-plus me-2"></i>Nuevo</a>
    </div>
    <!-- Sección para la tabla de registros -->
    <div>
        <table class="table table-hover align-middle">
            <thead class="table-dark">
                <tr class="row">
                    <th class="col-1 d-xxl-block d-none">C.I.F.</th> 
                    <th class="col">Nombre</th>
                    <th class="col-2 d-xxl-block d-none">Contacto</th>
                    <th class="col-1 d-xxl-block d-none">Tlfno.</th>
                    <th class="col-2 d-xxl-block d-none">Correo</th>
                    <th class="col text-end">Opciones</th>
                </tr>
            </thead>
            <tbody>
                <!-- Recorremos todos los registros pasados a la vista mediante el modelo para acceder a ellos -->
                @foreach ($promoters as $promoter)
                    <tr class="row">
                        <th class="col-1 d-xxl-block d-none">{{$promoter->cif}}</th>
                        <td class="col">{{$promoter->name}}</td>
                        <td class="col-2 d-xxl-block d-none">{{$promoter->contact}}</td>
                        <td class="col-1 d-xxl-block d-none">{{$promoter->phone}}</td>
                        <td class="col-2 d-xxl-block d-none">{{$promoter->mail}}</td>
                        <td class="col text-end">
                            <!-- Creamos cada uno de los botones de ver y editar de cada uno de los registros con sus datos correspondientes -->
                            <a class="btn btn-primary" href="{{route('promoters.show', $promoter)}}"><i class="fa-solid fa-eye"></i></a>
                            <a class="btn btn-warning" href="{{route('promoters.edit', $promoter)}}"><i class="fa-solid fa-pencil"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- Sección para el listado de páginas de registros -->
    <div class="d-flex justify-content-center m-2">
        {!!$promoters->links()!!}
    </div>
@endsection