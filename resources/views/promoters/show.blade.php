<!-- Plantilla de la que extiende la vista -->
@extends('layouts.app')
<!-- Titulo de la pestaña en la vista -->
@section('title', 'Promotor')
<!-- Sección del título de la vista -->
@section('header')
<!-- Título -->
    <h4 class="mb-0"><i class="fa-solid fa-helmet-safety me-2"></i>{{$promoter->name}}</h4>
    <!-- Información sobre el camino de dónde nos encontramos dentro de la aplicación -->
    <nav class="navbar-nav ms-auto d-none d-md-block" aria-label="breadcrumb">
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{route('promoters.index')}}">Promotores</a></li>
            <li class="breadcrumb-item active" aria-current="page">Datos del Promotor</li>
        </ol>
    </nav>
@endsection
<!-- Sección del contenido de la vista -->
@section('content')
    <div class="container mb-3">
        <div class="row">
            <div class="col-md-4 mb-3">
                <label for="cif" class="form-label">C.I.F.:</label>
                <input type="text" class="form-control" name="cif" value="{{$promoter->cif}}" disabled>
            </div>
            <div class="col-md-8 mb-3">
                <label for="name" class="form-label">Nombre:</label>
                <input type="text" class="form-control" name="name" value="{{$promoter->name}}" disabled>
            </div>
            <div class="col-md-6 mb-3">
                <label for="addresse" class="form-label">Dirección:</label>
                <input type="text" class="form-control" name="addresse" value="{{$promoter->addresse}}" disabled>
            </div>
            <div class="col-md-6 mb-3">
                <label for="city" class="form-label">Localidad:</label>
                <input type="text" class="form-control" name="city" value="{{$promoter->city}}" disabled>
            </div>
            <div class="col-md-4 mb-3">
                <label for="contact" class="form-label">Contacto:</label>
                <input type="text" class="form-control" name="contact" value="{{$promoter->contact}}" disabled>
            </div>
            <div class="col-md-4 mb-3">
                <label for="phone" class="form-label">Teléfono:</label>
                <input type="tel" class="form-control" name="phone" value="{{$promoter->phone}}" disabled>
            </div>
            <div class="col-md-4 mb-3">
                <label for="mail" class="form-label">Correo:</label>
                <input type="email" class="form-control" name="mail" value="{{$promoter->mail}}" disabled>
            </div>
            <div class="col-md-6 mb-3">
                <label for="created_at" class="form-label">Fecha de Creación:</label>
                <input type="text" class="form-control" name="created_at" value="{{$promoter->created_at->diffForHumans()}}" disabled>
            </div>
            <div class="col-md-6 mb-3">
                <label for="updated_at" class="form-label">Última Modificación:</label>
                <input type="text" class="form-control" name="updated_at" value="{{$promoter->updated_at->diffForHumans()}}" disabled>
            </div>
        </div>
        <!-- Sección para listar los proyectos del Promotor -->
        <div class="container mb-3">
            <fieldset>
                <legend>Proyectos del Promotor:</legend>
                <table class="table table-hover align-middle">
                    <thead class="table-dark">
                        <tr class="row">
                            <th class="col-2 d-xxl-block d-none">Código</th> 
                            <th class="col d-xxl-block d-none">Nombre</th> 
                            <th class="col-2 d-xxl-block d-none">Localidad</th> 
                            <th class="col-2 d-xxl-block d-none">Estado</th> 
                        </tr>
                    </thead>
                    <tbody>
                    <!-- Recorremos todos los registros pasados a la vista mediante el modelo para acceder a ellos -->
                    @forelse($promoter->projects as $project)
                    <tr class="row">
                        <th class="col-2 d-xxl-block d-none">{{$project->code}}</th>
                        <td class="col d-xxl-block d-none">{{$project->name}}</td>
                        <td class="col-2 d-xxl-block d-none">{{$project->city}}</td>
                        <td class="col-2 d-xxl-block d-none">{{$project->state->name}}</td>
                    </tr>
                    @empty
                    <tr class="row">
                        <td>Este promotor no tiene ningún proyecto</td>
                    </tr>
                    @endforelse
                    </tbody>
                </table>
            </fieldset>
        </div>
    </div>
        <!-- Sección para el listado de páginas de registros -->
    <div class="d-flex justify-content-center">
        <a href="{{route('promoters.index')}}" class="btn btn-primary"><i class="fa-solid fa-arrow-rotate-left me-2"></i>Volver</a>
    </div>
@endsection