<div class="col-md-4 mb-3">
    <label for="cif" class="form-label">C.I.F.:</label>
    <input type="text" class="form-control" name="cif" value="{{old('cif', $promoter->cif)}}" autofocus>
</div>
<div class="col-md-8 mb-3">
    <label for="name" class="form-label">Nombre:</label>
    <input type="text" class="form-control" name="name" value="{{old('name', $promoter->name)}}">
</div>
<div class="col-md-6 mb-3">
    <label for="addresse" class="form-label">Dirección:</label>
    <input type="text" class="form-control" name="addresse" value="{{old('addresse', $promoter->addresse)}}">
</div>
<div class="col-md-6 mb-3">
    <label for="city" class="form-label">Localidad:</label>
    <input type="text" class="form-control" name="city" value="{{old('city', $promoter->city)}}">
</div>
<div class="col-md-4 mb-3">
    <label for="contact" class="form-label">Contacto:</label>
    <input type="text" class="form-control" name="contact" value="{{old('contact', $promoter->contact)}}">
</div>
<div class="col-md-4 mb-3">
    <label for="phone" class="form-label">Teléfono:</label>
    <input type="tel" class="form-control" name="phone" value="{{old('phone', $promoter->phone)}}">
</div>
<div class="col-md-4 mb-3">
    <label for="mail" class="form-label">Correo:</label>
    <input type="email" class="form-control" name="mail" value="{{old('mail', $promoter->mail)}}">
</div>


