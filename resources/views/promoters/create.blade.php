<!-- Plantilla de la que extiende la vista -->
@extends('layouts.app')
<!-- Titulo de la pestaña en la vista -->
@section('title', 'Crear Promotor')
<!-- Sección del título de la vista -->
@section('header')
<!-- Título -->
<h4 class="mb-0"><i class="fa-solid fa-helmet-safety me-2"></i>Crear Promotor</h4>
<!-- Información sobre el camino de dónde nos encontramos dentro de la aplicación -->
<nav class="navbar-nav ms-auto d-none d-md-block" aria-label="breadcrumb">
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
        <li class="breadcrumb-item" aria-current="page"><a href="{{route('promoters.index')}}">Promotores</a></li>
        <li class="breadcrumb-item active" aria-current="page">Crear Promotor</li>
    </ol>
</nav>
@endsection
<!-- Sección del contenido de la vista -->
@section('content')
<div class="container">
    <!-- Formulario para crear los registros -->
    <form class="row" method="POST" action="{{route('promoters.store')}}" , enctype="multipart/form-data">
        @csrf
        <!-- Se crea una plantilla parcial con los inputs de los formularios para no tener que duplicarlos -->
        @include('promoters._form')
        <div class="d-flex justify-content-center m-2">
            <button type="submit" class="btn btn-success me-2"><i class="fa-solid fa-plus me-2"></i>Crear</button>
            <a href="{{route('promoters.index')}}" class="btn btn-secondary"><i class="fa-solid fa-xmark me-2"></i>Cancelar</a>
        </div>
    </form>
</div>
@endsection
