<fieldset class="row" disabled>
    <legend>Datos Generales:</legend>
    <div class="col-md-2 mb-3">
        <label for="code" class="form-label">Código:</label>
        <input type="text" class="form-control" name="code" value="{{$project->code}}">
    </div>
    <div class="col-md-5 mb-3">
        <label for="name" class="form-label">Nombre:</label>
        <input type="text" class="form-control" name="name" value="{{$project->name}}">
    </div>
    <div class="col-md-5 mb-3">
        <label for="city" class="form-label">Localidad:</label>
        <input type="text" class="form-control" name="city" value="{{$project->city}}">
    </div>
    <div class="col-md-12 mb-3">
        <label for="description" class="form-label">Descripción:</label>
        <textarea class="form-control" name="description" rows="3">{{$project->description}}</textarea>

    </div>
    <div class="col-md-6 mb-3">
        <label for="promoter_id" class="form-label">Promotor:</label>
        <input type="text" name="promoter_id" class="form-control" value="{{$project->promoter->name}}">
    </div>
    <div class="col-md-3 mb-3">
        <label for="orderDate" class="form-label">Fecha de Encargo:</label>
        <input type="date" class="form-control" name="orderDate" value="{{$project->orderDate}}">
    </div>
    <div class="col-md-6 mb-3">
        <label for="user_id" class="form-label">Técnico Responsable:</label>
        <input type="text" name="user_id" class="form-control" value="{{$project->user->name}}">
    </div>
    <div class="col-md-3 mb-3">
        <label for="state_id" class="form-label">Estado:</label>
        <input type="text" class="form-control" name="state_id" value="{{$project->state->name}}">
    </div>
</fieldset>
<fieldset class="row" disabled>
    <div class="col-md-3 mb-3">
        <label for="nPau" class="form-label">Nº PAU Totales:</label>
        <input type="number" class="form-control" name="nPau" min=2 value="{{$project->nPau}}">
    </div>
    <div class="col-md-3 mb-3">
        <label for="tipoEdif" class="form-label">Tipo de Edificio:</label>
        <input type="text" class="form-control" value="@if($project->tipoEdif === 'B')Viviendas en Bloque @else Unifamiliares @endif">

    </div>
    <div class="col-md-3 mb-3">
        <label for="numMaxPauVertical" class="form-label">PAU Máx. en la Vertical:</label>
        <input type="number" class="form-control" name="numMaxPauVertical" value="{{$project->numMaxPauVertical}}">
    </div>
    <div class="col-md-3 mb-3">
        <label for="numMaxPauPlanta" class="form-label">PAU Máx. en una Planta:</label>
        <input type="number" class="form-control" name="numMaxPauPlanta" value="{{$project->numMaxPauPlanta}}">
    </div>
</fieldset>
<fieldset class="row mt-5" disabled>
    <legend>Dimensionado de la I.C.T.:</legend>
    <div class="col-md-3 mb-3">
        <label for="arqExterior" class="form-label">Arqueta Exterior:</label>
        <input type="text" class="form-control" name="arqExterior" value="{{$project->arqExterior}}">
    </div>
    <div class="col-md-3 mb-3">
        <label for="canalExterior" class="form-label">Canalización Exterior:</label>
        <input type="text" class="form-control" name="canalExterior" value="{{$project->canalExterior}}">
    </div>
    <div class="col-md-3 mb-3">
        <label for="regEnlace" class="form-label">Registro de Enlace:</label>
        <input type="text" class="form-control" name="regEnlace" value="{{$project->regEnlace}}">
    </div>
    <div class="col-md-3 mb-3">
        <label for="canalEnlace" class="form-label">Canalización de Enlace:</label>
        <input type="text" class="form-control" name="canalEnlace" value="{{$project->canalEnlace}}">
    </div>
    <div class="col-md-4 mb-3">
        <label for="recintos" class="form-label">Recintos de Telecomunicaciones:</label>
        <input type="text" class="form-control" name="recintos" value="{{$project->recintos}}">
    </div>
    <div class="col-md-6 mb-3">
        <label for="canalPrincipal" class="form-label">Canalización Principal:</label>
        <input type="text" class="form-control" name="canalPrincipal" value="{{$project->canalPrincipal}}">
    </div>
    <div class="col-md-4 mb-3">
        <label for="regSecundario" class="form-label">Registros Secundario:</label>
        <input type="text" class="form-control" name="regSecundario" value="{{$project->regSecundario}}">
    </div>
    <div class="col-md-6 mb-3">
        <label for="canalSecundaria" class="form-label">Canalización Secundaria:</label>
        <input type="text" class="form-control" name="canalSecundaria" value="{{$project->canalSecundaria}}">
    </div>
</fieldset>
