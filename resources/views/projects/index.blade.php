<!-- Plantilla de la que extiende la vista -->
@extends('layouts.app')
<!-- Titulo de la pestaña en la vista -->
@section('title', 'Proyectos')
<!-- Sección del título de la vista -->
@section('header')
<!-- Título -->
<h4 class="mb-0"><i class="fas fa-pencil-ruler me-2"></i>Proyectos</h4>
    <!-- Información sobre el camino de dónde nos encontramos dentro de la aplicación -->
    <nav class="navbar-nav ms-auto" aria-label="breadcrumb">
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Proyectos</li>
        </ol>
    </nav>
@endsection
<!-- Sección del contenido de la vista -->
@section('content')
    <!-- Sección para el botón de nuevo registro -->
    @if(Auth::user()->isJefe())
    <div class="d-flex justify-content-end mb-2">
        <a class="btn btn-success" href="{{route('projects.create')}}"><i class="fa-solid fa-plus me-2"></i>Nuevo</a>
    </div>
    @endif
    <!-- Sección para la tabla de registros -->
    <div>
        <table class="table table-hover align-middle">
            <thead class="table-dark">
                <tr class="row">
                    <th class="col-1 d-xxl-block d-none">Código</th> 
                    <th class="col-xxl-4 col">Nombre</th>
                    <th class="col-2 d-xxl-block d-none">Localidad</th>
                    <th class="col-2 d-xxl-block d-none">Promotor</th>
                    <th class="col-1 d-xxl-block d-none">Estado</th>
                    <th class="col text-end">Opciones</th>
                </tr>
            </thead>
            <tbody>
                <!-- Recorremos todos los registros pasados a la vista mediante el modelo para acceder a ellos -->
                @foreach ($projects as $project)
                    <tr class="row">
                        <th class="col-1 d-xxl-block d-none">{{$project->code}}</th>
                        <td class="col-xxl-4 col">{{$project->name}}</td>
                        <td class="col-2 d-xxl-block d-none">{{$project->city}}</td>
                        <td class="col-2 d-xxl-block d-none">{{$project->promoter->name}}</td>
                        <td class="col-1 d-xxl-block d-none">{{$project->state->name}}</td>
                        <td class="col text-end">
                            <!-- Creamos cada uno de los botones de ver y editar de cada uno de los registros con sus datos correspondientes -->
                            <a class="btn btn-primary" href="{{route('projects.show', $project)}}"><i class="fa-solid fa-eye"></i></a>
                            <a class="btn btn-info" href="{{route('pdf.dataProject', $project)}}"><i class="fa-solid fa-print"></i></a>
                            @if(Auth::user()->isResponsable())
                            <a class="btn btn-warning" href="{{route('projects.edit', $project)}}"><i class="fa-solid fa-pencil"></i></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                <tr>
            </tbody>
        </table>
    </div>
    <!-- Sección para el listado de páginas de registros -->
    <div class="d-flex justify-content-center m-2">
        {!!$projects->links()!!}
    </div>
@endsection