<fieldset class="row">
    <div class="col-md-6 mb-3">
        <label for="name" class="form-label">Nombre:</label>
        <input type="text" class="form-control" name="name" value="{{old('name', $project->name)}}">
    </div>
    <div class="col-md-6 mb-3">
        <label for="city" class="form-label">Localidad:</label>
        <input type="text" class="form-control" name="city" value="{{old('city', $project->city)}}">
    </div>
    <div class="col-md-12 mb-3">
        <label for="description" class="form-label">Descripción:</label>
        <textarea name="description" rows="3" class="form-control">{{old('name', $project->description)}}</textarea>
    </div>
    <div class="col-md-6 mb-3">
        <label for="promoter_id" class="form-label">Promotor:</label>
        <select name="promoter_id" id="promoter_id" class="form-select">
            <option value="">Seleccione el promotor...</option>
            @foreach ($promoters as $promoter)
            <option value="{{$promoter->id}}"
                @if ($promoter->id == old('promoter_id', $project->promoter_id)) selected="selected" @endif
                >{{$promoter->cif}} | {{$promoter->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-3 mb-3">
        <label for="orderDate" class="form-label">Fecha de Encargo:</label>
        <input type="date" class="form-control" name="orderDate" value="{{old('orderDate', $project->orderDate)}}">
    </div>
    <div class="col-md-6 mb-3">
        <label for="user_id" class="form-label">Técnico Responsable:</label>
        <select name="user_id" id="user_id" class="form-select">
            <option value="">Seleccione un responsable...</option>
            @foreach ($responsables as $user)
            <option value="{{$user->id}}"
                @if ($user->id == old('user_id', $project->user_id)) selected="selected" @endif
                >{{$user->dni}} | {{$user->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-3 mb-3">
        <label for="state_id" class="form-label">Estado:</label>
        <select name="state_id" id="state_id" class="form-select">
            @foreach ($states as $state)
            <option value="{{$state->id}}"
                @if ($state->id == old('state_id', $project->state_id)) selected="selected" @endif
                >{{$state->name}}</option>
            @endforeach
        </select>
    </div>
</fieldset>
<fieldset class="row">
    <div class="col-md-3 mb-3">
        <label for="nPau" class="form-label">Nº PAU Totales:</label>
        <input type="number" class="form-control" name="nPau" min=2 value="{{old('nPau', $project->nPau)}}">
        <small><i class="fa-solid fa-circle-info me-2"></i>nº P.A.U. totales que tiene el edificio</small>
    </div>
    <div class="col-md-3 mb-3">
        <label for="tipoEdif" class="form-label">Tipo de Edificio:</label>
        <select name="tipoEdif" id="tipoEdif" class="form-select">
            <option value='B' @if ($project->tipoEdif == old('tipoEdif', 'B')) selected="selected" @endif>Viviendas en Bloque</option>
            <option value='U' @if ($project->tipoEdif == old('tipoEdif', 'U')) selected="selected" @endif>Unifamiliares</option>
        </select>
    </div>
    <div class="col-md-3 mb-3">
        <label for="numMaxPauVertical" class="form-label">PAU Máx. en la Vertical:</label>
        <input type="number" class="form-control" name="numMaxPauVertical" value="{{old('numMaxPauVertical', $project->numMaxPauVertical)}}">
        <small><i class="fa-solid fa-circle-info me-2"></i>acometidos en la vertical o ramal.</small>
    </div>
    <div class="col-md-3 mb-3">
        <label for="numMaxPauPlanta" class="form-label">PAU Máx. en una Planta:</label>
        <input type="number" class="form-control" name="numMaxPauPlanta" value="{{old('numMaxPauPlanta', $project->numMaxPauPlanta)}}">
        <small><i class="fa-solid fa-circle-info me-2"></i>acometidos en la planta o sub-ramal.</small>
    </div>
</fieldset>
