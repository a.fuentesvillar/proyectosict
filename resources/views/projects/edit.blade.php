<!-- Plantilla de la que extiende la vista -->
@extends('layouts.app')
<!-- Titulo de la pestaña en la vista -->
@section('title', 'Editar Proyecto')
<!-- Sección del título de la vista -->
@section('header')
<!-- Título -->
<h4 class="mb-0"><i class="fas fa-pencil-ruler me-2"></i>Editar {{$project->name}}</h4>
    <!-- Información sobre el camino de dónde nos encontramos dentro de la aplicación -->
    <nav class="navbar-nav ms-auto" aria-label="breadcrumb">
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{route('projects.index')}}">Proyectos</a></li>
            <li class="breadcrumb-item active" aria-current="page">Editar Proyecto</li>
        </ol>
    </nav>
@endsection
<!-- Sección del contenido de la vista -->
@section('content')
    <div class="container mb-3">
        <!-- Advertencia sobre zona de edición -->
        <div class="bg-warning rounded p-3 mb-2">
            <h5 class="mb-0">Zona de Edición de registros</h5>
        </div>
        <!-- Formulario para editar los registros -->
        <form class="row" method="POST" action="{{route('projects.update', $project)}}", enctype="multipart/form-data">
            @csrf
            @method('put')
            @include('projects._form')
            <div class="d-flex justify-content-center m-2">
                <button type="submit" class="btn btn-warning me-2"><i class="fa-solid fa-pencil me-2"></i>Editar</button>
                <a href="{{route('projects.show', $project)}}" class="btn btn-secondary"><i class="fa-solid fa-xmark me-2"></i>Cancelar</a>
            </div>
        </form>
    </div>
    <!-- Sección para el listado de todas las tareas que tiene el proyecto -->
    <div class="container mb-3">
        <fieldset>
            <legend>Tareas del proyecto:</legend>
            <table class="table table-hover align-middle">
                <thead class="table-dark">
                    <tr class="row">
                        <th class="col-1 d-xxl-block d-none">Código</th> 
                        <th class="col">Nombre</th>
                        <th class="col-2 d-xxl-block d-none">Usuario</th>
                        <th class="col-2 d-xxl-block d-none">Fecha Asignación</th>
                        <th class="col-2 d-xxl-block d-none">Estado</th>
                        <th class="col-1 d-xxl-block d-none">Horas Estimadas</th>
                        <th class="text-end col-2">Opciones</th>
                    </tr>
                </thead>
                <tbody>
                <!-- Recorremos todos los registros pasados a la vista mediante el modelo para acceder a ellos -->
                @forelse($projectTasks as $t)
                <tr class="row">
                    <th class="col-1 d-xxl-block d-none">{{$t->task->id}}</th>
                    <td class="col">{{$t->task->name}}</td>
                    <td class="col-2 d-xxl-block d-none">{{$t->user->name}}</td>
                    <td class="col">{{$t->asigDate}}</td>
                    <td class="col-2 d-xxl-block d-none">{{$t->state->name}}</td>
                    <td class="col-1 d-xxl-block d-none">{{$t->estimatedHours}}</td>
                    <td class="text-end col-2">
                        <!-- Creamos cada uno de los botones de ver y editar de cada uno de los registros con sus datos correspondientes -->
                        <a class="btn btn-primary" href="{{route('projectsTasks.show', [$project, $t->task])}}"><i class="fa-solid fa-eye"></i></a>
                        <a class="btn btn-warning" href="{{route('projectsTasks.edit', [$project, $t->task])}}"><i class="fa-solid fa-pencil"></i></a>
                        <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modalBorrar"><i class="fa-solid fa-trash"></i></button>
                        <!-- Modal emergente para crear un aviso antes de borrar el registro -->
                        <div class="modal fade" id="modalBorrar" tabindex="-1" aria-labelledby="modalBorrarLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger text-light">
                                        <h1 class="modal-title fs-5" id="modalBorrarLabel">Eliminar asignación de Tarea a Proyecto</h1>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <div>Esta a punto de eliminiar la tarea de {{$t->task->name}} del proyecto {{$t->project->code}}, {{$t->project->name}}. ¿Esta seguro?</div>
                                    </div>
                                    <div class="modal-footer">
                                        <form method="POST" action="{{route('projectsTasks.destroy', [$project, $t->task])}}", enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="_method" value="delete">
                                            <button type="button" class="btn btn-secondary me-2" data-bs-dismiss="modal"><i class="fa-solid fa-xmark me-2"></i>Cancelar</button>
                                            <button type="submit" class="btn btn-outline-danger"><i class="fa-solid fa-trash me-2"></i>Eliminar</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </td>
                </tr>
                @empty
                <tr class="row">
                    <td>Este proyecto no tiene ninguna tarea asignada.</td>
                </tr>
                @endforelse
                </tbody>
            </table>
            <!-- Creamos el formulario para agregar tareas a los proyectos -->
            <h6 class="table"><b>Asignar Nueva Tarea a Proyecto:</b></h6>
            <form method="GET" action="{{route('projectsTasks.create', $project)}}", enctype="multipart/form-data">
                <div class="input-group mb-3">
                    <select class="col form-select" name="task_id">
                        @foreach ($tasks as $task)
                        <option value="{{$task->id}}">{{$task->id}} | {{$task->name}}</option>
                        @endforeach
                    </select>
                    <button type="submit" class="btn btn-success"><i class="fa-solid fa-plus"></i></button>
                </div>
            </form>
        </fieldset>
    </div>
@endsection