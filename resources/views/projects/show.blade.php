<!-- Plantilla de la que extiende la vista -->
@extends('layouts.app')
<!-- Titulo de la pestaña en la vista -->
@section('title', 'Proyectos')
<!-- Sección del título de la vista -->
@section('header')
<!-- Título -->
<h4 class="mb-0"><i class="fas fa-pencil-ruler me-2"></i>{{$project->name}}</h4>
    <!-- Información sobre el camino de dónde nos encontramos dentro de la aplicación -->
    <nav class="navbar-nav ms-auto d-none d-md-block" aria-label="breadcrumb">
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{route('projects.index')}}">Proyectos</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$project->code}}</li>
        </ol>
    </nav>
@endsection
<!-- Sección del contenido de la vista -->
@section('content')
    <div class="container mb-3">
        @include('projects._formShow')
    </div>
    <!-- Sección para listar las tareas que realizan los departamentos -->
    <div class="container mb-3">
        <fieldset>
            <legend>Tareas del proyecto:</legend>
            <table class="table table-hover align-middle">
                <thead class="table-dark">
                    <tr class="row">
                        <th class="col-1 d-xxl-block d-none">Código</th> 
                        <th class="col">Nombre</th>
                        <th class="col-2 d-xxl-block d-none">Usuario</th>
                        <th class="col-2 d-xxl-block d-none">Fecha Asignación</th>
                        <th class="col-2 d-xxl-block d-none">Estado</th>
                        <th class="col-2 d-xxl-block d-none">Horas Estimadas</th>
                    </tr>
                </thead>
                <tbody>
                <!-- Recorremos todos los registros pasados a la vista mediante el modelo para acceder a ellos -->
                @forelse($projectTasks as $t)
                <tr class="row">
                    <th class="col-1 d-xxl-block d-none">{{$t->task->id}}</th>
                    <td class="col">{{$t->task->name}}</td>
                    <td class="col-2 d-xxl-block d-none">{{$t->user->name}}</td>
                    <td class="col">{{$t->asigDate}}</td>
                    <td class="col-2 d-xxl-block d-none">{{$t->state->name}}</td>
                    <td class="col-2 d-xxl-block d-none">{{$t->estimatedHours}}</td>
                </tr>
                @empty
                <tr class="row">
                    <td>Este proyecto no tiene ninguna tarea asignada.</td>
                </tr>
                @endforelse
                </tbody>
            </table>
        </fieldset>
    </div>
    <!-- Sección para el listado de páginas de registros -->
    <div class="d-flex justify-content-center">
        <a class="btn btn-info me-2" href="{{route('pdf.dataProject', $project)}}"><i class="fa-solid fa-print me-2"></i>Imprimir</a>
        <a class="btn btn-primary" href="{{route('projects.index')}}"><i class="fa-solid fa-arrow-rotate-left me-2"></i>Volver</a>
    </div>
@endsection