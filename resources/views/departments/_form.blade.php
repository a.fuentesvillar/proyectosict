<div class="col-md-6 mb-3">
    <label for="name" class="form-label">Nombre:</label>
    <input type="text" class="form-control" name="name" placeholder="Nombre del departamento" value="{{old('name', $department->name)}}">
</div>
<div class="col-md-12 mb-3">
    <label for="description" class="form-label">Descripción:</label>
    <textarea name="description" rows="5" class="form-control" placeholder="Descripción">{{old('name', $department->description)}}</textarea>
</div>