<!-- Plantilla de la que extiende la vista -->
@extends('layouts.app')
<!-- Titulo de la pestaña en la vista -->
@section('title', 'Departamento')
<!-- Sección del título de la vista -->
@section('header')
<!-- Título -->
<h4 class="mb-0"><i class="fa-solid fa-building-user me-2"></i>{{$department->name}}</h4>
    <!-- Información sobre el camino de dónde nos encontramos dentro de la aplicación -->
    <nav class="navbar-nav ms-auto d-none d-md-block" aria-label="breadcrumb">
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{route('departments.index')}}">Departamentos</a></li>
            <li class="breadcrumb-item active" aria-current="page">Datos del Departamento</li>
        </ol>
    </nav>
@endsection
<!-- Sección del contenido de la vista -->
@section('content')
    <div class="container mb-3">
        <div class="row">
            <div class="col-md-6 mb-3">
                <label for="name" class="form-label">Nombre:</label>
                <input type="text" class="form-control" name="name" value="{{$department->name}}" disabled>
            </div>
            <div class="col-md-12 mb-3">
                <label for="description" class="form-label">Descripción:</label>
                <textarea name="description" rows="5" class="form-control" disabled>{{$department->description}}</textarea>
            </div>
            <div class="col-md-6 mb-3">
                <label for="created_at" class="form-label">Fecha de Creación:</label>
                <input type="text" class="form-control" name="created_at" value="{{$department->created_at->diffForHumans()}}" disabled>
            </div>
            <div class="col-md-6 mb-3">
                <label for="updated_at" class="form-label">Última Modificación:</label>
                <input type="text" class="form-control" name="updated_at" value="{{$department->updated_at->diffForHumans()}}" disabled>
            </div>
        </div>
    </div>
    <!-- Sección para listar las tareas que realizan los departamentos -->
    <div class="container mb-3">
        <div class="card">
            <div class="card-header bg-dark text-light">
                <span class="card-title fs-5">Tareas del Departamento</span>
            </div>
            <div class="card-body">
                <ul class="list-group">
                    @forelse($department->tasks as $task)
                    <li class="list-group-item">{{$task->id}} | {{$task->name}}</li>
                    @empty
                    <span>Este departamento no tiene ninguna tarea asignada.</span>
                    @endforelse
                </ul>
            </div>
        </div>
    </div>
    <!-- Sección para listar los usuarios que pertenecen al departamento -->
    <div class="container mb-3">
        <div class="card">
            <div class="card-header bg-dark text-light">
                <span class="card-title fs-5">Usuarios</span>
            </div>
            <div class="card-body">
                <ul class="list-group">
                    @forelse($department->users as $user)
                    <li class="list-group-item">{{$user->dni}} | {{$user->name}} {{$user->surname}}</li>
                    @empty
                    <span>Este departamento no tiene ningún usuario asignado.</span>
                    @endforelse
                </ul>
            </div>
        </div>
    </div>
    <!-- Pie de la sección -->
    <div class="d-flex justify-content-center">
        <a class="btn btn-primary me-2" href="{{route('departments.index')}}" ><i class="fa-solid fa-arrow-rotate-left me-2"></i>Volver</a>
        <a class="btn btn-warning" href="{{route('departments.edit', $department)}}"><i class="fa-solid fa-pencil me-2"></i>Editar</a>
    </div>
@endsection