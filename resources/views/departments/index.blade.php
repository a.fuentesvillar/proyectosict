<!-- Plantilla de la que extiende la vista -->
@extends('layouts.app')
<!-- Titulo de la pestaña en la vista -->
@section('title', 'Departamentos')
<!-- Sección del título de la vista -->
@section('header')
<!-- Título -->
<h4 class="mb-0"><i class="fa-solid fa-building-user me-2"></i>Departamentos</h4>
    <!-- Información sobre el camino de dónde nos encontramos dentro de la aplicación -->
    <nav class="navbar-nav ms-auto" aria-label="breadcrumb">
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Departamentos</li>
        </ol>
    </nav>
@endsection
<!-- Sección del contenido de la vista -->
@section('content')
<!-- Sección para el botón de nuevo registro -->
<div class="d-flex justify-content-end mb-2">
    <a class="btn btn-success" href="{{route('departments.create')}}"><i class="fa-solid fa-plus me-2"></i>Nuevo</a>
</div>
<!-- Sección para la tabla de registros -->
<div>
    <table class="table table-hover align-middle">
        <thead class="table-dark">
            <tr class="row">
                <th class="col-1 d-xxl-block d-none">Id</th> 
                <th class="col">Nombre</th>
                <th class="col-2 d-xxl-block d-none">Descripción</th>
                <th class="col text-end">Opciones</th>
            </tr>
        </thead>
        <tbody>
            <!-- Recorremos todos los registros pasados a la vista mediante el modelo para acceder a ellos -->
            @foreach ($departments as $department)
                <tr class="row">
                    <th class="col-1 d-xxl-block d-none">{{$department->id}}</th>
                    <td class="col">{{$department->name}}</td>
                    <td class="col-5 d-xxl-block d-none">{{$department->description}}</td>
                    <td class="col text-end">
                        <!-- Creamos el botón para ver los detalles del registro -->
                        <a class="btn btn-primary" href="{{route('departments.show', $department)}}"><i class="fa-solid fa-eye"></i></a>
                    </td>
                </tr>
            @endforeach
            <tr>
        </tbody>
    </table>
</div>
<!-- Sección para el listado de páginas de registros -->
<div class="d-flex justify-content-center m-2">
    {!!$departments->links()!!}
</div>
@endsection
