<!-- Plantilla de la que extiende la vista -->
@extends('layouts.app')
<!-- Titulo de la pestaña en la vista -->
@section('title', 'Editar Departamento')
<!-- Sección del título de la vista -->
@section('header')
    <!-- Título -->
    <h4 class="mb-0"><i class="fa-solid fa-building-user me-2"></i>Editar {{$department->name}}</h4>
    <!-- Información sobre el camino de dónde nos encontramos dentro de la aplicación -->
    <nav class="navbar-nav ms-auto" aria-label="breadcrumb">
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{route('departments.index')}}">Departamentos</a></li>
            <li class="breadcrumb-item active" aria-current="page">Editar Departamento</li>
        </ol>
    </nav>
@endsection
<!-- Sección del contenido de la vista -->
@section('content')
    <div class="container mb-3">
        <!-- Advertencia sobre zona de edición -->
        <div class="bg-warning rounded p-3 mb-2">
            <h5 class="mb-0">Zona de Edición de registros</h5>
        </div>
        <!-- Formulario para editar los registros -->
        <form class="row" method="POST" action="{{route('departments.update', $department)}}", enctype="multipart/form-data">
            @csrf
            @method('put')
            @include('departments._form')
            <div class="d-flex justify-content-center m-2">
                <button type="submit" class="btn btn-warning me-1"><i class="fa-solid fa-pencil me-2"></i>Editar</button>
                <a href="{{route('departments.show', $department)}}" class="btn btn-secondary me-5"><i class="fa-solid fa-xmark me-2"></i>Cancelar</a>
            </div>
        </form>
    </div>
    <!-- Sección para el listado de todas las tareas que puede realizar el departamento -->
    <div class="container">
        <div class="card">
            <div class="card-header bg-dark text-light">
                <span class="card-title fs-5">Tareas del Departamento</span>
            </div>
            <div class="card-body">
                <div class="card-text">
                    <table class="table table-hover align-middle">
                        <thead class="table">
                            <tr class="row">
                                <th class="col-1 d-xxl-block d-none">Código</th> 
                                <th class="col">Nombre</th>
                                <th class="col-5 d-xxl-block d-none">Descripción</th>
                                <th class="text-end col">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Recorremos todos los registros pasados a la vista mediante el modelo para acceder a ellos -->
                            @forelse($department->tasks as $task)
                            <tr class="row">
                                <th class="col-1 d-xxl-block d-none">{{$task->id}}</th>
                                <td class="col">{{$task->name}}</td>
                                <td class="col-5 d-xxl-block d-none">{{$task->description}}</td>
                                <td class="text-end col">
                                    <!-- Creamos el botón borrar para quitar la asignación de la tarea al departamento -->
                                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modalBorrar"><i class="fa-solid fa-trash"></i></button>
                                    <!-- Modal emergente para crear un aviso antes de borrar el registro -->
                                    <div class="modal fade" id="modalBorrar" tabindex="-1" aria-labelledby="modalBorrarLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header bg-danger text-light">
                                                    <h1 class="modal-title fs-5" id="modalBorrarLabel">Eliminar asignación de Tarea a Departamento</h1>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <div>Esta a punto de eliminiar la tarea de {{$task->name}} del departamento {{$department->name}}. ¿Esta seguro?</div>
                                                </div>
                                                <div class="modal-footer">
                                                    <form method="POST" action="{{route('departmentTask.destroy', [$department, $task])}}", enctype="multipart/form-data">
                                                        @csrf
                                                        <input type="hidden" name="_method" value="delete">
                                                        <button type="button" class="btn btn-secondary me-2" data-bs-dismiss="modal"><i class="fa-solid fa-xmark me-2"></i>Cancelar</button>
                                                        <button type="submit" class="btn btn-outline-danger"><i class="fa-solid fa-trash me-2"></i>Eliminar</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @empty
                            <tr class="row">
                                <td>Este departamento no tiene ninguna tarea asignada.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    <!-- Creamos el formulario para agregar tareas a los departamentos -->
                    <h6 class="table"><b>Agregar Tarea:</b></h6>
                    <form method="POST" action="{{route('departmentTask.store', $department)}}", enctype="multipart/form-data">
                        @csrf
                        <div class="input-group mb-3">
                            <select class="col form-select" name="task_id">
                                <option value="">Selecciona una tarea...</option>
                                @foreach ($tasks as $task)
                                <option value="{{$task->id}}">{{$task->id}} | {{$task->name}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="department_id" value="{{$department->id}}">
                            <button type="submit" class="btn btn-success"><i class="fa-solid fa-plus"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection