@extends('layouts.app')

@section('content')
    <!-- Saludo de bienvenida -->
    <div class="container mb-3 text-center">
        <ul class="list-group bg-secondary text-light">
            <h4 class="m-2">¡Bienvenido a {{config('app.name')}}!</h4>
        </ul>
    </div>
    <!-- Sección para la sección de noticias y avisos -->
    <div class="container mb-3">
        <div class="card border-info">
            <div class="card-header bg-info border-info text-light">
                <span class="card-title fs-5"><i class="fa-solid fa-circle-info me-2"></i>Información del Usuario</span>
            </div>
            <div class="card-body">
                <ul class="list-group">
                    <li class="list-group-item list-group-item-primary">Nombre completo: {{$user->name}} {{$user->surname}}</li>
                    <li class="list-group-item list-group-item-primary">D.N.I.: {{$user->dni}}</li>
                    <li class="list-group-item list-group-item-primary">Correo: {{$user->email}}</li>
                    <li class="list-group-item list-group-item-primary">Departamento: {{$user->department->name}}</li>
                    <li class="list-group-item list-group-item-primary">Fecha de creación: {{$user->created_at->diffForHumans()}}</li>
                  </ul>
            </div>
        </div>
    </div>
    <!-- Sección para las tareas pendientes -->
    <div class="container mb-3">
        <div class="card border-warning">
            <div class="card-header bg-warning border-warning">
                <span class="card-title fs-5"><i class="fa-solid fa-clipboard-check me-2"></i>Tareas Pendientes</span>
            </div>
            <div class="card-body">
                <table class="table table-hover align-middle">
                    <thead class="table">
                        <tr class="row">
                            <th class="col-1 d-xxl-block d-none">Código Proy.</th>
                            <th class="col d-xxl-block d-none"> Nombre Proy.</th>
                            <th class="col-2 d-xxl-block d-none">Tarea</th>
                            <th class="col-2 d-xxl-block d-none">Fecha Asignación</th>
                            <th class="col-2 d-xxl-block d-none">Horas Estimadas</th>
                            <th class="col d-xxl-block d-none"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="row">
                            @forelse($tasks as $task)
                            <th class="col-1 d-xxl-block d-none">{{$task->project->code}}</th>
                            <td class="col d-xxl-block d-none">{{$task->project->name}}</td>
                            <td class="col-2 d-xxl-block d-none">{{$task->task->name}}</td>
                            <td class="col-2 d-xxl-block d-none">{{$task->asigDate}}</td>
                            <td class="col-2 d-xxl-block d-none">{{$task->estimatedHours}}</td>
                            <td class="col d-xxl-block d-none text-end">
                                <a class="btn btn-warning" href="{{route('projectsTasks.edit', [$task->project, $task->task])}}"><i class="fa-solid fa-pencil"></i></a>
                            </td>
                            @empty
                            <td class="col">No tienes tareas pendientes.</td>
                            @endforelse
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Sección para los proyectos de los que el usuario es responsable -->
    <div class="container mb-3">
        <div class="card border-primary">
            <div class="card-header bg-primary border-primary text-light">
                <span class="card-title fs-5"><i class="fa-solid fa-pencil-ruler me-2"></i>Proyectos de los que eres responsable</span>
            </div>
            <div class="card-body">
                <table class="table table-hover align-middle">
                    <thead class="table">
                        <tr class="row">
                            <th class="col-1 d-xxl-block d-none">Código Proy.</th>
                            <th class="col d-xxl-block d-none"> Nombre Proy.</th>
                            <th class="col-2 d-xxl-block d-none">Tarea</th>
                            <th class="col-2 d-xxl-block d-none">Fecha Asignación</th>
                            <th class="col-2 d-xxl-block d-none">Horas Estimadas</th>
                            <th class="col d-xxl-block d-none"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="row">
                            @forelse($projects as $project)
                            <th class="col-1 d-xxl-block d-none">{{$project->code}}</th>
                            <td class="col d-xxl-block d-none">{{$project->name}}</td>
                            <td class="col-2 d-xxl-block d-none">{{$project->city}}</td>
                            <td class="col-2 d-xxl-block d-none">{{$project->promoter->name}}</td>
                            <td class="col-2 d-xxl-block d-none">{{$project->orderDate}}</td>
                            <td class="col d-xxl-block d-none text-end">
                                <a class="btn btn-primary" href="{{route('projects.show', $project)}}"><i class="fa-solid fa-eye"></i></a>
                            </td>
                            @empty
                            <td class="col">No tienes proyectos asignados.</td>
                            @endforelse
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
