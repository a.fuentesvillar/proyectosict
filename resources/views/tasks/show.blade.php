<!-- Plantilla de la que extiende la vista -->
@extends('layouts.app')
<!-- Titulo de la pestaña en la vista -->
@section('title', 'Tarea')
<!-- Sección del título de la vista -->
@section('header')
<!-- Título -->
<h4 class="mb-0"><i class="fa-solid fa-clipboard-check me-2"></i>{{$task->name}}</h4>
<!-- Información sobre el camino de dónde nos encontramos dentro de la aplicación -->
<nav class="navbar-nav ms-auto" aria-label="breadcrumb">
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
        <li class="breadcrumb-item" aria-current="page"><a href="{{route('tasks.index')}}">Tareas</a></li>
        <li class="breadcrumb-item active" aria-current="page">Datos de la Tarea</li>
    </ol>
</nav>
@endsection
<!-- Sección del contenido de la vista -->
@section('content')
    <div class="container mb-3">
        <div class="row">
            <div class="col-md-6 mb-3">
                <label for="name" class="form-label">Nombre:</label>
                <input type="text" class="form-control" name="name" value="{{$task->name}}" disabled>
            </div>
            <div class="col-md-12 mb-3">
                <label for="description" class="form-label">Descripción:</label>
                <textarea name="description" rows="5" class="form-control" disabled>{{$task->description}}</textarea>
            </div>
            <div class="col-md-6 mb-3">
                <label for="created_at" class="form-label">Fecha de Creación:</label>
                <input type="text" class="form-control" name="created_at" value="{{$task->created_at->diffForHumans()}}" disabled>
            </div>
            <div class="col-md-6 mb-3">
                <label for="updated_at" class="form-label">Última Modificación:</label>
                <input type="text" class="form-control" name="updated_at" value="{{$task->updated_at->diffForHumans()}}" disabled>
            </div>
        </div>
    </div>
    <!-- Sección para listar los departamentos que pueden realizar la tarea -->
    <div class="container mb-3">
        <div class="card">
            <div class="card-header bg-dark text-light">
                <span class="card-title fs-5">Departamentos responsables</span>
            </div>
            <div class="card-body">
                <ul class="list-group">
                    @forelse($task->departments as $department)
                    <li class="list-group-item">{{$department->id}} | {{$department->name}}</li>
                    @empty
                    <span>Esta tarea no tiene ningún departamento responsable</span>
                    @endforelse
                </ul>
            </div>
        </div>
    </div>
    <!-- Pie de la sección -->
    <div class="d-flex justify-content-center">
        <a class="btn btn-primary me-2" href="{{route('tasks.index')}}"><i class="fa-solid fa-arrow-rotate-left me-2"></i>Volver</a>
        <a class="btn btn-warning" href="{{route('tasks.edit', $task)}}"><i class="fa-solid fa-pencil me-2"></i>Editar</a>
    </div>
@endsection