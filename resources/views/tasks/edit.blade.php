<!-- Plantilla de la que extiende la vista -->
@extends('layouts.app')
<!-- Titulo de la pestaña en la vista -->
@section('title', 'Editar Tarea')
<!-- Sección del título de la vista -->
@section('header')
<!-- Título -->
<h4 class="mb-0"><i class="fa-solid fa-clipboard-check me-2"></i>Editar {{$task->name}}</h4>
    <!-- Información sobre el camino de dónde nos encontramos dentro de la aplicación -->
    <nav class="navbar-nav ms-auto" aria-label="breadcrumb">
        <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{route('tasks.index')}}">Tareas</a></li>
            <li class="breadcrumb-item active" aria-current="page">Editar Tarea</li>
        </ol>
    </nav>
@endsection
<!-- Sección del contenido de la vista -->
@section('content')
    <div class="container mb-3">
        <!-- Advertencia sobre zona de edición -->
        <div class="bg-warning rounded p-3 mb-2">
            <h5 class="mb-0">Zona de Edición de registros</h5>
        </div>
        <!-- Formulario para editar los registros -->
        <form class="row" method="POST" action="{{route('tasks.update', $task)}}", enctype="multipart/form-data">
            @csrf
            @method('put')
            @include('tasks._form')
            <div class="d-flex justify-content-center m-2">
                <button type="submit" class="btn btn-warning me-2"><i class="fa-solid fa-pencil me-2"></i>Editar</button>
                <a href="{{route('tasks.show', $task)}}" class="btn btn-secondary me-2"><i class="fa-solid fa-xmark me-2"></i>Cancelar</a>
            </div>
        </form>
    </div>
@endsection