<?php 

namespace App\ClassesAFV;

use Illuminate\Database\Eloquent\Model;

// CLASE ABSTRACTA PARA EL CÁLCULO DE LA INFRAESTRUCTURA COMÚN
// DE TELECOMUNICACIONES ICT PARA EDIFICIOS SEGÚN R.D. 346/2011
// Alejandro Fuentes Villar
abstract class Ict extends Model
{
    /**
     * Cálculo de Dimensiones de la ARQUETA EXTERIOR.
     * Sus dimensiones se condicionan en función del número de PAU's totales que se acaometen en el edificio.
     */
    public function setArqExterior(){
        $pau = $this->nPau;
        if ($pau <= 20){
            $this->arqExterior = "1x(400x400x400mm)";
        }elseif ($pau <= 100){
            $this->arqExterior = "1x(600x600x800mm)";
        }else{
            $this->arqExterior = "1x(800x700x820mm)";
        }
    }

    /**
     * Cálculo de Cantidad de conductos para la CANALIZACIÓN EXTERIOR.
     * El número de tubos se calcula en función del número de PAU's totales que se acaometen en el edificio.
     */
    public function setCanalExterior(){
        $pau = $this->nPau;
        if ($pau <= 4){
            $this->canalExterior = "3xØ63mm (2 TBA+STDP; 1 Reserva)";
        }elseif($pau <= 20){
            $this->canalExterior = "4xØ63mm (2 TBA+STDP; 2 Reserva)";
        }elseif($pau <= 40){
            $this->canalExterior = "5xØ63mm (3 TBA+STDP; 2 Reserva)";
        }else{
            $this->canalExterior = "6xØ63mm (4 TBA+STDP; 2 Reserva)";
        }
    }

    /**
     * Cálculo del REGISTRO DE ENLACE.
     * Actualmente en el Reglamento sólo se contempla un único Registro de dimensiones fijas.
     */
    public function setRegEnlace(){
        $this->regEnlace = "1x(450x450x120mm)";
    }

    /**
     * Cálculo de la cantidad de conductos para la CANALIZACIÓN DE ENLACE.
     * El número de conductos es igual al número de conductos de canalización Exterior.
     * Tan sólo cambia sus dimensiones.
     */
    public function setCanalEnlace(){
        $pau = $this->nPau;
        if ($pau <= 4){
            $this->canalEnlace = "3xØ50mm";
        }elseif($pau <= 20){
            $this->canalEnlace = "4xØ50mm";
        }elseif($pau <= 40){
            $this->canalEnlace = "5xØ50mm";
        }else{
            $this->canalEnlace = "6xØ50mm";
        }
    }

    /**
     * Cálculo de cantidad y dimensiones de RECINTOS DE TELECOMUNICACIONES.
     * Se calcula en función del Número de PAU Totales y del tipo de Edificio.
     */
    public function setRecintos(){
        $pau = $this->nPau;
        $edificio = $this->tipoEdif;
        
        switch ($edificio){
            case 'B': // En caso de que sean Viviendas en Bloque
                if($pau <=10){
                    $this->recintos = "1 Recinto [2000x1000x500mm.]";
                }elseif($pau <=20){
                    $this->recintos = "2 Recintos [2000x1000x500mm.]";
                }elseif($pau <=30){
                    $this->recintos = "2 Recintos [2000x1500x500mm.]";
                }elseif($pau <=45){
                    $this->recintos = "2 Recintos [2000x2000x500mm.]";
                }else{
                    $this->recintos = "2 Recintos [2300x2000x2000mm.]";
                }
                break;
            case 'U': // En caso de que sean Viviendas Unifamiliares
                if($pau <=10){
                    $this->recintos = "1 Recinto [2000x1000x500mm.]";
                }elseif($pau <=20){
                    $this->recintos = "1 Recinto [2000x1500x500mm.]";
                }else{
                    $this->recintos = "1 Recinto [2300x2000x2000mm.]";
                }
                break;
        }
    }

    /**
     * Cálculo de cantidad de conductos de CANALIZACIÓN PRINCIPAL.
     * El número de conductos sólo depende del número de PAU total que se acometen en la vertical o ramal.
     */
    public function setCanalPrincipal(){
        $pauVertical = $this->numMaxPauVertical;

        if($pauVertical<=10){
            $this->canalPrincipal = "5xØ50mm (1 RTV; 1 Pares/Par Trenzado; 1 Coaxial; 1 F.O.; 1 Reserva)";
        }elseif($pauVertical<=20){
            $this->canalPrincipal = "6xØ50mm (1 RTV; 1 Pares/Par Trenzado; 2 Coaxial; 1 F.O.; 1 Reserva)";
        }elseif($pauVertical<=30){
            $this->canalPrincipal = "7xØ50mm (1 RTV; 1 Pares/Par Trenzado; 2 Coaxial; 1 F.O.; 2 Reserva)";
        }else{
            $this->canalPrincipal = "8xØ50mm (1 RTV; 1 Pares/Par Trenzado; 2 Coaxial; 1 F.O.; 3 Reserva)";
        }
    }

    /**
     * Dimensionado de los REGISTROS SECUNDARIOS
     * Las dimensiones de estos registros tienen en cuenta varios factores;
     * el número total de PAU que acometen en la vertical o ramal,
     * el número de PAU máximo por planta,
     * y el tipo de edificio.
     */
    public function setRegSecundario(){
        $pauVertical = $this->numMaxPauVertical;
        $pauPlanta = $this->numMaxPauPlanta;
        $edificio = $this->tipoEdif;

        switch ($edificio){
            case 'B':
                if($pauVertical>=30){
                    $this->regSecundario = "550x1000x150mm.";
                }elseif($pauVertical>=20){
                    $this->regSecundario = "500x700x150mm.";
                }else{
                    if($pauPlanta>4){
                        $this->regSecundario = "500x700x150mm.";
                    }else{
                        $this->regSecundario = "450x450x150mm.";
                    }
                }
                break;
            case 'U':
                $this->regSecundario = "450x450x150mm.";
                break;
        }
    }

    /**
     * Dimensionado de CANALIZACIÓN SECUNDARIA.
     * Para este caso, el Reglamento sólo tiene en cuenta un único dimensionado.
     */
    public function setCanalSecundaria(){
        $this->canalSecundaria = "4xØ25mm (1 RTV; 1 Pares/Par Trenzado; 1 Coaxial; 1 Reserva)";
    }

    /**
     * Función para llamar al CÁLCULO de todos los elementos que componen la ICT
     */
    public function resultadoICT(){
        // Red de Alimentación
        $this->setArqExterior();
        $this->setCanalExterior();
        $this->setRegEnlace();
        $this->setCanalEnlace();
        // Recintos
        $this->setRecintos();
        // Red de Distribución
        $this->setCanalPrincipal();
        $this->setRegSecundario();
        $this->setCanalSecundaria();
    }
}