<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        // Importamos la clase para utilizar las pginaciones de bootstrap
        Paginator::useBootstrap();
        // Ponemos la localización de la librería de Carbon para fechas en Español
        Carbon::setLocale('es');
    }
}
