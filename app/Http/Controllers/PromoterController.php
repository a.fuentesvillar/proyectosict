<?php

namespace App\Http\Controllers;

use App\Http\Requests\PromoterRequest;
use App\Models\Promoter;
use Illuminate\Http\Request;

class PromoterController extends Controller
{    
    /**
     * Función para mostar la vista del listado general de registros
     */
    public function index(){
        $promoters = Promoter::orderBy('id')
            ->paginate(10);
        // Devolvemos la vista
        return view('promoters.index', compact('promoters'));
    }

    /**
     * Función para guardar el registro creado, validado por el Request correspondiente
     */
    public function store(PromoterRequest $request){
        Promoter::create($request->validated());
        // Redirigimos la aplicación a la vista índice junto con un mensaje de confirmación de elemento creado.
        return redirect()
            ->route('promoters.index')
            ->with('status', $request["name"].' se ha creado correctamente.');
    }

    /**
     * Función para mostrar la vista de crear registro
     */
    public function create(){
        // Creamos un objeto Promoter nuevo vacío para tener disponibles los campos a rellenar
        $promoter = new Promoter;
        // Devolvemos la vista y le pasamos el objeto vacío creado
        return view('promoters.create', compact('promoter'));
    }

    /**
     * Función para mostrar la vista de ver registro
     */
    public function show(Promoter $promoter){
        // Mostramos la vista con el objeto que le hemos pasado anteriormente
        return view('promoters.show', compact('promoter'));
    }

    /**
     * Función para actualizar un registro, validado por el Request correspondiente
     */
    public function update(PromoterRequest $request, Promoter $promoter){
        $promoter->update($request->validated());
        // Redirigimos la aplicación a la vista índice junto con un mensaje de confirmación de elemento modificado.
        return redirect()
            ->route('promoters.index')
            ->with('status', $promoter['name'].' se ha actualizado correctamente.');
    }

    /**
     * Función para mostrar la vista de Editar registro. Le pasamos el elemento a editar
     */
    public function edit(Promoter $promoter){
        return view ('promoters.edit', compact('promoter'));
    }
}
