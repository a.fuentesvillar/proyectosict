<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Models\DepartmentTask;

class DepartmentTaskController extends Controller
{
    /**
     * Función para guardar la asignación de la tarea al departamento
     * Ya que sólo va a se necesario validar en este punto los datos,
     * se validan directamente en el controlador sin necesidad de un Request
     * específico.
     */
    public function store(Request $request, $department){
        // Validamos los datos
        $data = $request->validate([
            'department_id' => 'required|integer',
            'task_id' => 'required|integer'
        ]);
        // Creamos la asignación de la tarea
        DepartmentTask::create($data);
        // Redirigimos la aplicación a la vista índice junto con un mensaje de confirmación de elemento creado.
        return redirect()
            ->route('departments.edit', $department)
            ->with('status', 'La tarea se ha asignado al departamento correctamente.');
    }

    /**
     * Función para eliminar la asignación de la tarea al departamento.
     */
    public function destroy(Department $department, Task $task){
        // Buscamos la asigancion de la tarea
        $departmentTask = DepartmentTask::where('department_id', $department->id)
            ->where('task_id', $task->id);
        // Borramos la asignación de la tarea
        $departmentTask->delete();
        // Redirigimos la aplicación a la vista índice junto con un mensaje de confirmación de elemento eliminado.
        return redirect()
            ->route('departments.edit', $department)
            ->with('status', 'La tarea se ha eliminado del departamento correctamente.');
    }
}
