<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Project;
use Illuminate\Support\Facades\App;

class PdfController extends Controller
{
    /**
     * Función que se ejecuta al invocar al controlador
     */
    public function __invoke(Project $project){
        // Almacenamos el usuario responsable del proyecto
        $user = User::findOrFail($project->user_id);
        // Almacenamos la fecha de hoy
        $date = Carbon::today();
        // Creamos la instancia de la biblioteca para generar PDF
        $dompdf = App::make("dompdf.wrapper");
        // Cargamos la vista creada del PDF con los datos pasados
        $dompdf->loadView('pdf.dataProject', [
            'project' => $project,
            'user' => $user,
            'date' => $date
        ]);
        // Devolvemos el informe en PDF
        return $dompdf->stream($project->code.'.pdf');
    }
}
