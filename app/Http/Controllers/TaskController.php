<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use App\Http\Requests\TaskRequest;

class TaskController extends Controller
{
    /**
     * Función para mostar la vista del listado general de registros
     */
    public function index(){
        $tasks = Task::orderBy('id')
            ->paginate(5);
        return view('tasks.index', compact('tasks'));
    }

    /**
     * Función para guardar el registro creado, validado por el Request correspondiente
     */
    public function store(TaskRequest $request){
        Task::create($request->validated());
        // Redirigimos la aplicación a la vista índice junto con un mensaje de confirmación de elemento creado.
        return redirect()
            ->route('tasks.index')
            ->with('status', 'La tarea '.$request->name.' se ha creado correctamente.');
    }

    /**
     * Función para mostrar la vista de crear registro
     */
    public function create(){
        // Creamos un objeto Task nuevo vacío para tener disponibles los campos a rellenar
        $task = new Task;
        // Devolvemos la vista y le pasamos el objeto vacío creado
        return view('tasks.create', compact('task'));
    }

    /**
     * Función para mostrar la vista de ver registro
     */
    public function show(Task $task){
        // Mostramos la vista con el objeto que le hemos pasado anteriormente
        return view('tasks.show', compact('task'));
    }

    /**
     * Función para actualizar un registro, validado por el Request correspondiente
     */
    public function update(TaskRequest $request, Task $task){
        $task->update($request->validated());
        // Redirigimos la aplicación a la vista índice junto con un mensaje de confirmación de elemento modificado.
        return redirect()
            ->route('tasks.index')
            ->with('status', 'La tarea '.$task->name.' se ha actualizado correctamente.');
    }

    /**
     * Función para mostrar la vista de Editar registro. Le pasamos el elemento a editar
     */
    public function edit(Task $task){
        return view ('tasks.edit', compact('task'));
    }
}
