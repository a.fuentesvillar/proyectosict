<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\UserUpdatePassRequest;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Función para mostar la vista del listado general de registros
     */
    public function index(){
        $users=User::orderBy('name')
            ->paginate(5);
        // Devolvemos la vista con los registros
        return view('users.index', compact('users'));
    }

    /**
     * Función para mostrar la vista de crear registro
     */
    public function create(){
        // Creamos un objeto Project nuevo vacío para tener disponibles los campos a rellenar.
        $user = new User;
        // Listamos todos los departamentos para tenerlos disponibles en el select
        $departments = Department::orderBy('id', 'ASC')
            ->get();
        // Devolvemos la vista y le pasamos el objeto vacío creado Project y el listado de promotores
        return view('users.create', compact(
            'user',
            'departments'
        ));
    }

    /**
     * Función para guardar el registro creado, validado por el Request correspondiente
     */
    public function store(UserStoreRequest $request){
        // Creamos el usuario con los datos validados
        $user = new User($request->validated());
        // Encriptamos la clave para almacenarla de forma segura en la base de datos
        $user->password = bcrypt($user->password);
        // Guardamos el usuario en la base de datos
        $user->save();
        // Devolvemos la vista general de usuarios con un mensaje de confirmación
        return redirect()
            ->route('users.index')
            ->with('status', $user->name.' se ha creado correctamente.');
    }

    /**
     * Función para mostrar la vista de ver registro
     */
    public function show(User $user){
        return view('users.show', compact('user'));
    }

    /**
     * Función para mostrar la vista de Editar registro. Le pasamos el elemento a editar
     */
    public function edit(User $user){
        // Listamos todos los departamentos para tenerlos disponibles en el select
        $departments = Department::orderBy('id', 'ASC')
            ->get();
        // Devolvemos la vista 
        return view('users.edit', compact(
            'user',
            'departments'
        ));
    }

    /**
     * Función para actualizar un registro, validado por el Request correspondiente
     */
    public function update(UserUpdateRequest $request, User $user){
        $user->update($request->validated());
        // Devolvemos la vista general de usuarios con un mensaje de confirmación
        return redirect()
            ->route('users.index')
            ->with('status', $user->name.' se ha actualizado correctamente.');
    }

    /**
     * Función para mostrar la vista de cambio de contraseña
     */
    public function editPass(User $user){
        return view('users.editPass', compact('user'));
    }

    /**
     * Función para cambiar la contraseña de un usuario
     */
    public function updatePass(UserUpdatePassRequest $request, User $user){
        // Leemos los datos pasados por formulario
        $data = $request->validated();
        // Encriptamos la contraseña para guardarla de forma segura en la base de datos
        $data['password'] = bcrypt($data['password']);
        // Actualizamos el usuario
        $user->update($data);
        // Devolvemos la vista general de usuarios con un mensaje de confirmación
        return redirect()
            ->route('users.index')
            ->with('status', $user->name.' se ha actualizado correctamente.');
    }

}
