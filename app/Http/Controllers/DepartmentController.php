<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Models\DepartmentTask;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\DepartmentRequest;

class DepartmentController extends Controller
{
    /**
     * Función para mostar la vista del listado general de registros
     */
    public function index(){
        $departments = Department::orderBy('id')
            ->paginate(10);
        // Devolvemos la vista
        return view('departments.index', compact('departments'));
    }

    /**
     * Función para guardar el registro creado, validado por el Request correspondiente
     */
    public function store(DepartmentRequest $request){
        Department::create($request->validated());
        // Redirigimos la aplicación a la vista índice junto con un mensaje de confirmación de elemento creado.
        return redirect()
            ->route('departments.index')
            ->with('status', $request['name'].' se ha creado correctamente.');
    }

    /**
     * Función para mostrar la vista de crear registro
     */
    public function create(){
        // Creamos un objeto Department nuevo vacío para tener disponibles los campos a rellenar
        $department = new Department;
        // Devolvemos la vista y le pasamos el objeto vacío creado
        return view('departments.create', compact('department'));
    }

    /**
     * Función para mostrar la vista de ver registro
     */
    public function show(Department $department){
        // Mostramos la vista con el objeto que le hemos pasado anteriormente
        return view('departments.show', compact('department'));
    }

    /**
     * Función para actualizar un registro, validado por el Request correspondiente
     */
    public function update(DepartmentRequest $request, Department $department){
        $department->update($request->validated());
        // Redirigimos la aplicación a la vista índice junto con un mensaje de confirmación de elemento modificado.
        return redirect()
            ->route('departments.index')
            ->with('status', $department['name'].' se ha actualizado correctamente.');
    }

    /**
     * Función para mostrar la vista de Editar registro. Le pasamos el elemento a editar
     */
    public function edit(Department $department){
        /**  
         * Mediante la consulta, determinamos las tareas que aún no tiene asignado el
         * departamento para mostrarlas en el select y así no dar la posibilidad de
         * repetir asignaciones
        */
        $department_id = $department->id;
        $tasks = DB::table('tasks')
                    ->whereNotIn('id', function($query) use ($department_id) {
                        $query->select('task_id')
                              ->from('department_task')
                              ->where('department_id', '=', $department_id);
                    })
                    ->get();
        // Redirigimos la aplicación a la vista y le pasamos los parámetros.
        return view ('departments.edit', compact('department','tasks'));
    }
}
