<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\User;
use App\Models\State;
use App\Models\Project;
use App\Models\ProjectTask;
use Illuminate\Http\Request;
use App\Models\DepartmentTask;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ProjectTaskRequest;

class ProjectTaskController extends Controller
{
    /***
     * Función para guardar el registro
     */
    public function store(ProjectTaskRequest $request, $project){
        ProjectTask::create($request->validated());
        // Devolvemos la vista de editar proyecto con un mensaje de confirmación
        return redirect()
            ->route('projects.show', $project)
            ->with('status', 'La Tarea se ha asignado al proyecto correctamente.');
    }
    /**
     * Función para mostrar la vista de crear registro
     */
    public function create(Request $request, Project $project){
        // Seleccionamos la tarea indicada
        $task = Task::findOrFail($request->task_id);
        // Seleccionamos los departamentos que puede hacer la tarea quehemos indicado
        $departments = DepartmentTask::where('task_id','=', $task->id)
            ->get();
        // Guardamos en un array los id de los departamentos para luego pasarlos a la consulta principal
        $query = array();
        foreach($departments as $i){
            $query[] = $i->department_id;
        }
        // Obtenenmos los usuarios de los departamentos autorizados.
        $users = User::whereIn('department_id', $query)
            ->get();  
        // Obtenemos los estados que puede tener una tarea asignada   
        $states = State::orderBy('id', 'ASC')
            ->get();
        // Creamos un objeto vacío para tener disponibles los campos a rellenar
        $projectTask = new ProjectTask();
        // Devolvemos la vista
        return view('projectsTask.create', compact(
            'projectTask',
            'project',
            'task',
            'users',
            'states'
        ));
    }
    /**
     * Función para mostrar la vista de ver registro
     */
    public function show(Project $project, Task $task){
        // Obtenemos la tarea asiganada al proyecto
        $projectTask = ProjectTask::where('task_id', $task->id)
            ->where('project_id', $project->id)
            ->first();
        // Obtenemos el listado de usuarios para obtener el nombre del usuario
        $users = User::orderBy('id', 'ASC')
            ->get();
        // Obtenemos los estados de tarea asignada para obtener sus datos             
        $states = State::orderBy('id', 'ASC')
            ->get();
        // Devolvemos la vista con un mensaje de confirmación
        return view('projectsTask.show', compact(
            'projectTask',
            'project',
            'task',
            'users',
            'states'
        ));
    }

    /**
     * Función para actualizar el registro
     */
    public function update(ProjectTaskRequest $request, Project $project, Task $task){
        // Obtenemos la tarea asiganada al proyecto
        $projectTask = ProjectTask::where('task_id',$task->id)
            ->where('project_id', $project->id)
            ->first();
        // Actualizamos el registro con los datos validados
        $projectTask->update($request->validated());
        // Devolvemos la vista con un mensaje de confirmación
        return redirect()
            ->route('projects.show', $project)
            ->with('status', 'La Tarea se ha actualizado y asignado al proyecto correctamente.');
    }

    /**
     * Función para borrar el registro
     */
    public function destroy(Project $project, Task $task){
    // Buscamos la asigancion de la tarea
    $projectTask = ProjectTask::where('task_id', $task->id)
        ->where('project_id', $project->id)
        ->first();
    // Borramos la asignación de la tarea
    $projectTask->delete();
    // Redirigimos la aplicación a la vista índice junto con un mensaje de confirmación de elemento eliminado.
    return redirect()
        ->route('projects.edit', $project)
        ->with('status', 'La Tarea se ha eliminado del proyecto correctamente.');
    }

    /**
     * Función para mostrar la vista de edición de registro
     */
    public function edit(Project $project, Task $task){
                // Obtenemos la tarea asiganada al proyecto
        $projectTask = ProjectTask::where('task_id', $task->id)
            ->where('project_id', $project->id)
            ->first();
        // Obtenemos el listado de usuarios
        $users = User::orderBy('id', 'ASC')
            ->get();
        // Obtenemos el listado de estados que pueden tener una tarea asignada
        $states = State::orderBy('id', 'ASC')
            ->get();
        // Devolvemos la vista
        return view('projectsTask.edit', compact(
            'projectTask',
            'project',
            'task',
            'users',
            'states'
        ));
    }
}
