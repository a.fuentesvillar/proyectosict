<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\State;
use App\Models\Project;
use App\Models\Promoter;
use App\Models\ProjectTask;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ProjectRequest;

class ProjectController extends Controller
{
    /**
     * Función para generar automáticamente el código interno del Proyecto.
     */
    function lastProject(){
        // Primero obtenemos el año actual gracias a la librería incluida en Laravel Carbon.
        $year=Carbon::now()->format("Y");
        // Listamos sólo los proyectos que coincidan con el año actual y los ordenamos de forma descendente.
        // Luego seleccionamos el primero. Así obtenemos el último proyecto del año actual.
        $n=Project::where('code',"LIKE", '%'.$year)->orderBy('code', 'DESC')->first();
        // Comprobamos que hay proyectos. De lo contrario, el último es el 0.
        if($n){
            $last=(int)$n->code;
        }else{
            $last=0;
        }
        // Creamos un string de 3 dígitos con el último valor existente.
        $newProject=str_pad((string)++$last, 3, "0", STR_PAD_LEFT);
        // Por último, devolvemos el código de proyecto, que se compone de 3 dígitos, un guión y el año actual
        // Ej: 025-2023
        return ($newProject."-".$year);
    }


    /**
     * Función para mostar la vista del listado general de registros
     */
    public function index(){
        $projects = Project::orderBy('code', 'DESC')
            ->paginate(10);
        return view('projects.index', compact('projects'));       
    }

    /**
     * Función para guardar el registro creado, validado por el Request correspondiente
     */
    public function store(ProjectRequest $request){
    $project = new Project($request->validated());
    // Ahora, aprovechamos la clase abstracta ICT que personalmente hemos creado para nuestro poryecto
    // y utilizamos el método resultadosICT para hacer el dimensionado de la ICT.
    $project->resultadoICT();
    // Calculamos el código interno del proyecto para pasarlo despues al formulario.
    $project->code = $this->lastProject();
    // Por último, actualizamos el proyecto con el dimensionado calculado.
    $project->save();
    // Redirigimos la aplicación a la vista índice junto con un mensaje de confirmación de elemento creado.
    return redirect()
        ->route('projects.show', $project)
        ->with('status', 'El Proyecto '.$project->name.' se ha creado y dimensionado correctamente.');
    }

    /**
     * Función para mostrar la vista de crear registro
     */
    public function create(){
        // Creamos un objeto Project nuevo vacío para tener disponibles los campos a rellenar.
        $project = new Project;
        // Listamos todos los promotores y se los pasamos a la vista para tenerlos disponible en el 
        // select que utilizaremos para asignar el promotor al proyecto
        $promoters = Promoter::orderBy('name', 'ASC')
            ->get();
        // Listamos todos los estados y se los pasamos a la vista para tenerlos disponible en el 
        // select que utilizaremos para asignarlo al proyecto
        $states = State::orderBy('id', 'ASC')
            ->get();
        // Mostramos la vista con el objeto que le hemos pasado anteriormente
        $responsables = User::where('department_id','3')
            ->orderBy('id','ASC')
                ->get();
        // Devolvemos la vista y le pasamos el objeto vacío creado Project y el listado de promotores
        return view('projects.create', compact(
            'project',
            'promoters',
            'states',
            'responsables'));
    }

    /**
     * Función para mostrar la vista de ver registro
     */
    public function show(Project $project){
        $projectTasks = ProjectTask::where('project_id','=', $project->id)
            ->get();
        // Listamos todos los estados y se los pasamos a la vista para tenerlos disponible en el 
        // select que utilizaremos para asignarlo al proyecto      
        $responsables = User::where('department_id','3')->orderBy('id','ASC')
            ->get();
        // Devolvemos la vista y le pasamos el objeto vacío creado Project y el listado de promotores
        return view('projects.show', compact(
            'project',
            'projectTasks',
            'responsables'));
    }

    /**
     * Función para actualizar un registro, validado por el Request correspondiente
     */
    public function update(ProjectRequest $request, Project $project){
        $project->update($request->validated());
        // Ahora, aprovechamos la clase abstracta ICT que personalmente hemos creado para nuestro poryecto
        // y utilizamos el método resultadosICT para actualizar el predimensionado.
        $project->resultadoICT();
        // Por último, actualizamos el proyecto con el predimensionado calculado.
        $project->save();
        // Redirigimos la aplicación a la vista índice junto con un mensaje de confirmación de elemento modificado.
        return redirect()
            ->route('projects.show', $project)
            ->with('status', 'El Proyecto '.$project->name.' se ha actualizado y dimensionado correctamente.');
    }

    /**
     * Función para mostrar la vista de Editar registro. Le pasamos el elemento a editar
     */
    public function edit(Project $project){
        $projectTasks = ProjectTask::where('project_id','=', $project->id)->get();
        // Listamos todos los promotores para incluirlo en el select
        $promoters = Promoter::orderBy('name', 'ASC')
            ->get(); 
        // Listamos todos los estados que puede tener el proyecto para incluirlos en el select
        $states = State::orderBy('id', 'ASC')
            ->get();
        // Listamos todos los responsables para incluirlos en el select
        $responsables = User::where('department_id','3')->orderBy('id','ASC')
            ->get();
        // Listamos todas las tareas que no tiene asignado el proyecto para incluirlas en el select
        $tasks = DB::table('tasks')
            ->whereNotIn('id', function($query) use ($project) {
                $query->select('task_id')
                    ->from('project_task')
                    ->where('project_id', '=', $project->id);
            })
            ->get();;
        // Devolvemos la vista y le pasamos el objeto vacío creado Project junto todos los datos de los select
        return view ('projects.edit', compact(
            'project',
            'projectTasks',
            'promoters',
            'states',
            'responsables', 
            'tasks'
        ));
    }
}
