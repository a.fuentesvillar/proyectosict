<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ProjectTask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Función para Muestra la vista Home.
     */
    public function __invoke()
    {
        // Almacenamos los datos del usuario logeado
        $user = Auth::user();
        // Almacenamos las tareas del usuario logeado que están pendientes
        $tasks = ProjectTask::where('user_id', $user->id)
            ->where('state_id',1)
            ->orderBy('asigDate','ASC')
            ->get();
        // Almacenamos los proyectos que tienen como responsable el usuario logeado
        $projects = Project::where('user_id', $user->id)->get();
        // Devolvemos la vista con los parametros adjuntos
        return view('home', compact(
            'user',
            'tasks',
            'projects'
        ));
    }
}
