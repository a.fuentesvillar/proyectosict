<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $administrador = User::where('id',1)->first();
        if (Auth::user()->department->id === $administrador->id) {
            return $next($request);
        } else {
            return redirect()
                ->back()
                ->with('status', 'Solo el usuario '.$administrador->department->name.' pueden acceder a este recurso.');
        }
    }
}
