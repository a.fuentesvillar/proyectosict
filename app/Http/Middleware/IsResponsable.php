<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class IsResponsable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $jefeDep = User::where('id',2)->first();
        $responsable = User::where('id',3)->first();
        if (Auth::user()->department->id === $responsable->id | Auth::user()->department->id === $jefeDep->id) {
            return $next($request);
        } else {
            return redirect()
                ->back()
                ->with('status', 'Usted no está autorizado para acceder a este recurso. Póngase en contacto con el administrador');
        }
    }
}
