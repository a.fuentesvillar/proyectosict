<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'city' => 'required',
            'description' => 'required',
            'promoter_id' => 'required|integer',
            'orderDate' => 'nullable|date',
            'user_id' => 'required|integer',
            'state_id' => 'required|integer',
            'nPau' => 'required|integer',
            'tipoEdif' => [
                'required',
                Rule::in(['B','U'])
            ],
            'numMaxPauVertical' => 'required|integer',
            'numMaxPauPlanta' => 'required|integer'
        ];
    }
    public function messages(){
        return [
            'promoter_id.required' => 'El campo promotor es obligatorio.',
            'user_id.required' => 'El campo responsable es obligatorio.',
            'state_id.required' => 'El campo estado es obligatorio.',
            'nPau.required' => 'El campo nº PAU totales es obligatorio.',
            'tipoEdif.required' => 'El campo tipo de edificio es obligatorio.',
        ];
    }
}
