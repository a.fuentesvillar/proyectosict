<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PromoterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $cif = $this->route('cif');
        return [
            'cif' => [
                'required',
                'size:9',
                // Para que no de errores a la hora de actualizar el registro
                // es necesario especificar que ignore el registro actual
                // de esta forma puede actualizarlo
                Rule::unique('promoters', 'cif')->ignore($this->promoter)
                ],
            'name' => 'required',
            'addresse' => 'required',
            'city' => 'required',
            'contact' => 'nullable',
            'phone' => 'required|size:9',
            'mail' => 'required|email'
        ];
    }
    public function messages(){
        return [
            'addresse.required' => 'El campo dirección es obligatorio.',
            'city.required' => 'El campo localidad es obligatorio.',
            'mail.required' => 'El campo correo es obligatorio.',
        ];
    }
}
