<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'project_id' => 'required|integer',
            'task_id' => 'required|integer',
            'user_id'  => 'required|integer',
            'asigDate' => 'required|date',
            'estimatedHours'  => 'required',
            'state_id' => 'required|integer',
            'doneDate' => 'nullable|date',
            'realHours' => 'nullable',
            'description' => 'nullable'
        ];
    }
    public function messages(){
        return [
            'promoter_id.required' => 'El campo promotor es obligatorio.',
            'task_id.required' => 'El campo tarea es obligatorio.',
            'user_id.required' => 'El campo responsable es obligatorio.',
            'asigDate.required' => 'El campo fecha de asignación es obligatorio.',
            'estimatedHours.required' => 'El campo horas estimadas es obligatorio.',
            'state_id.required' => 'El campo estado es obligatorio.',
        ];
    }
}
