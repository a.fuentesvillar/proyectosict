<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'surname' => 'required',
            'dni' => [
                'required',
                Rule::unique('users', 'dni')->ignore($this->user)
                ],
            'email' => [
                'required',
                Rule::unique('users', 'email')->ignore($this->user)
                ],
            'password' => 'required|confirmed|min:6|max:30',
            'department_id' => 'required|integer'
        ];
    }
    public function messages(){
        return [
            'department_id.required' => 'El campo departamento es obligatorio.',
        ];
    }
}
