<?php

namespace App\Models;

use App\ClassesAFV\Ict;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Project extends Ict
{
    use HasFactory;
    
    /**
     *  Indicamos los campos de la base de datos que son editables en el modelo
     */
    protected $fillable = [
        'code',
        'name',
        'description',
        'city',
        'promoter_id',
        'orderDate',
        'user_id',
        'state_id',
        'nPau',
        'tipoEdif',
        'numMaxPauVertical',
        'numMaxPauPlanta',
        'arqExterior'
    ];

    /**
     * Función para obtener una ruta amigable cuando se haga mención al modelo Project. 
     * Con esta función en vez de tomar el "id" de la tabla projects para obtener 
     * la url del recurso vamos a utilizar otro campo, en este caso el campo "code"
     */
    public function getRouteKeyName()
    {
        return 'code';
    }

    /**
     * Función para obtener la relación inversa entre Project y Promoter
     * De esta forma se obtiene las columnas de la tabla promoters
     */
    public function promoter(){
        return $this->belongsTo(Promoter::class);
    }

    /**
     * Creamos una función para obtener las tareas que contiene
     * una proyecto en concreto, obtenida mediante la relación
     * entre las tablas task (tareas) y projects (proyectos)
     */
    public function tasks(){
        return $this->belongsToMany(Task::class)
            ->using(projectTask::class)
            ->withPivot(['user_id','estimatedHours'])
            ->withTimestamps();
    }

    public function users(){
        return $this->belongsToMany(User::class, 'project_task');
    }

    public function state(){
        return $this->belongsTo(State::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
