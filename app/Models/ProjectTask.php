<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProjectTask extends Pivot
{
    use HasFactory;

    /**
     * Indicamos mediante la variable $table la tabla de datos intermedia
     * a los que hace referencia el modelo para la obtención de datos
     */
    protected $table = 'project_task';

    /**
     *  Indicamos los campos de la base de datos
     *  que son editables en el modelo
     */
    protected $fillable = [
            'project_id',
            'task_id',
            'user_id',
            'asigDate',
            'estimatedHours',
            'state_id',
            'doneDate',
            'realHours',
            'description'
    ];

    public function project(){
        return $this->belongsTo(Project::class);
    }

    public function task(){
        return $this->belongsTo(Task::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function state(){
        return $this->belongsTo(State::class);
    }

}
