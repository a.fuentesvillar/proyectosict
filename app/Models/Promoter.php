<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Promoter extends Model
{
    use HasFactory;

    /**
     *  Indicamos los campos de la base de datos
     *  que son editables en el modelo
     */
    protected $fillable = [
        'cif',
        'name',
        'addresse',
        'city',
        'contact',
        'phone',
        'mail'
    ];

    /**
     * Función para obtener una ruta amigable cuando se haga mención al modelo. 
     * Con esta función en vez de tomar el "id" de la tabla projects para obtener 
     * la url del recurso vamos a utilizar otro campo, en este caso el campo "cif"
     */
    public function getRouteKeyName()
    {
        return 'cif';
    }

    public function projects(){
        return $this->hasMany(Project::class);
    }

}
