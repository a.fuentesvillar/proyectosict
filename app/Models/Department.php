<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    /**
     *  Indicamos los campos de la base de datos que son editables en el modelo
     */
    protected $fillable = [
        'name',
        'description'
    ];

    /**
     * Función para obtener una ruta amigable cuando se haga mención al modelo Project. 
     * Con esta función en vez de tomar el "id" de la tabla projects para obtener 
     * la url del recurso vamos a utilizar otro campo, en este caso el campo "code"
     */
    public function getRouteKeyName()
    {
        return 'name';
    }

    /**
     * Creamos una función para obtener las tareas que pueden
     * realizar un departamento en concreto, obtenida mediante la relación
     * entre las tablas task (tareas) y departent (departamentos)
     */
    public function tasks(){
        return $this->belongsToMany(Task::class);
    }

    public function users(){
        return $this->hasMany(User::class);
    }
}
