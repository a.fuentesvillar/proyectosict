<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'surname',
        'dni',
        'email',
        'password',
        'department_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Función para obtener una ruta amigable cuando se haga mención al modelo. 
     * Con esta función en vez de tomar el "id" de la tabla projects para obtener 
     * la url del recurso vamos a utilizar otro campo, en este caso el campo "dni"
     */
    public function getRouteKeyName()
    {
        return 'dni';
    }

    /**
     * Función para obtener la relación inversa entre User y Department
     * De esta forma se obtiene las columnas de la tabla departments
     */
    public function department(){
        return $this->belongsTo(Department::class);
    }

    public function projects(){
        return $this->belongsToMany(Project::class);
    }

    public function isAdmin(){
        if ($this->department->id == 1){
            return true;
        }else{
            return false;
        }
    }

    public function isJefe(){
        if ($this->department->id == 2){
            return true;
        }else{
            return false;
        }
    }

    public function isResponsable(){
        if ($this->department->id == 2 | $this->department->id == 3){
            return true;
        }else{
            return false;
        }
    }
}
