<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    /**
     * Indicamos los campos que son editables desde la aplicación
     */
    protected $fillable = [
        'name',
        'description'
    ];

    /**
     * Creamos una función para obtener los departamentos que pueden
     * realizar una tarea en concreto, obtenida mediante la relación
     * entre las tablas task (tareas) y departent (departamentos)
     */
    public function departments(){
        return $this->belongsToMany(Department::class);
    }

    /**
     * Creamos una función para obtener los proyectos que contienen
     * una determinada tarea, obtenida mediante la relación
     * entre las tablas task (tareas) y projects (proyectos)
     */
    public function projects(){
        return $this->belongsToMany(Project::class)
            ->using(projectTask::class);
    }
    
    public function users(){
        return $this->belongsToMany(User::class)
            ->using(projectTask::class);
    }
}
