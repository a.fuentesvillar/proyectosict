<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepartmentTask extends Model
{
    use HasFactory;

    /**
     * Indicamos mediante la variable $table la tabla de datos
     * a los que hace referencia el modelo para la obtención de datos
     */
    protected $table = 'department_task';

    /**
     * Indicamos los campos que son editables desde la aplicación
     */
    protected $fillable = [
        'department_id',
        'task_id'
    ];

    public function tasks(){
        return $this->belongsToMany(Task::class);
    }
}
