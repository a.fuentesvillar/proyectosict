<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;

    /**
     * Indicamos los campos que son editables desde la aplicación
     */
    protected $fillable = [
        'name'
    ];
}
