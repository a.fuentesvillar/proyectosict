<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Task;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        DB::table('tasks')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');

        $task = new Task;
        $task->name = "Cálculos Justificativos";
        $task->description = "Cálculos Justificativos";        
        $task->created_at = Carbon::now();
        $task->updated_at = Carbon::now();
        $task->save();

        $task = new Task;
        $task->name = "Planos de Situación";
        $task->description = "Planos de Situación";        
        $task->created_at = Carbon::now();
        $task->updated_at = Carbon::now();
        $task->save();

        $task = new Task;
        $task->name = "Planos de Planta y Distribución";
        $task->description = "Planos de Planta y Distribución";        
        $task->created_at = Carbon::now();
        $task->updated_at = Carbon::now();
        $task->save();

        $task = new Task;
        $task->name = "Planos de Esquemas de Principios";
        $task->description = "Planos de Esquemas de Principios";        
        $task->created_at = Carbon::now();
        $task->updated_at = Carbon::now();
        $task->save();

        $task = new Task;
        $task->name = "Memoria Descriptiva";
        $task->description = "Memoria Descriptiva";        
        $task->created_at = Carbon::now();
        $task->updated_at = Carbon::now();
        $task->save();

        $task = new Task;
        $task->name = "Memoria Justificativa";
        $task->description = "Memoria Justificativa";        
        $task->created_at = Carbon::now();
        $task->updated_at = Carbon::now();
        $task->save();

        $task = new Task;
        $task->name = "Pliego de Prescripciones Técnicas";
        $task->description = "Pliego de Prescripciones Técnicas";        
        $task->created_at = Carbon::now();
        $task->updated_at = Carbon::now();
        $task->save();

        $task = new Task;
        $task->name = "Mediciones y Presupuesto";
        $task->description = "Mediciones y Presupuesto";        
        $task->created_at = Carbon::now();
        $task->updated_at = Carbon::now();
        $task->save();

        $task = new Task;
        $task->name = "Estudio / Básico de Seguridad y Salud";
        $task->description = "Estudio / Básico de Seguridad y Salud";        
        $task->created_at = Carbon::now();
        $task->updated_at = Carbon::now();
        $task->save();

        $task = new Task;
        $task->name = "Publicación, Impresión y Encuadernado";
        $task->description = "Publicación, Impresión y Encuadernado";        
        $task->created_at = Carbon::now();
        $task->updated_at = Carbon::now();
        $task->save();
    }
}
