<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        
        $user = new User;
        $user->name = "Alejandro";
        $user->surname = "Fuentes";
        $user->dni = "11222333A";
        $user->email = "admin@correo.com";
        $user->password = bcrypt("admin");
        $user->department_id = 1;
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->save();

        $user = new User;
        $user->name = "Tony";
        $user->surname = "Stark";
        $user->dni = "00112233B";
        $user->email = "jefe@correo.com";
        $user->password = bcrypt("jefe");
        $user->department_id = 2;
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->save();

        $user = new User;
        $user->name = "Bruce";
        $user->surname = "Banner";
        $user->dni = "22998844Q";
        $user->email = "responsable@correo.com";
        $user->password = bcrypt("responsable");
        $user->department_id = 3;
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->save();

        $user = new User;
        $user->name = "Steve";
        $user->surname = "Rogers";
        $user->dni = "99664477D";
        $user->email = "ingeniero@correo.com";
        $user->password = bcrypt("ingeniero");
        $user->department_id = 4;
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->save();

        $user = new User;
        $user->name = "Scott";
        $user->surname = "Lang";
        $user->dni = "66889944W";
        $user->email = "delineante@correo.com";
        $user->password = bcrypt("delineante");
        $user->department_id = 5;
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->save();

        $user = new User;
        $user->name = "Stephen";
        $user->surname = "Strange";
        $user->dni = "25252525C";
        $user->email = "auxiliar@correo.com";
        $user->password = bcrypt("auxiliar");
        $user->department_id = 6;
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->save();
    }
}
