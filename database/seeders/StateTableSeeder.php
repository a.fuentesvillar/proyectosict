<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\State;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        State::truncate();

        $promoter = new State;
        $promoter->name = "PENDIENTE";
        $promoter->created_at = Carbon::now();
        $promoter->updated_at = Carbon::now();
        $promoter->save();

        $promoter = new State;
        $promoter->name = "EJECUTADO";
        $promoter->created_at = Carbon::now();
        $promoter->updated_at = Carbon::now();
        $promoter->save();

        $promoter = new State;
        $promoter->name = "ANULADO";
        $promoter->created_at = Carbon::now();
        $promoter->updated_at = Carbon::now();
        $promoter->save();
    }
}
