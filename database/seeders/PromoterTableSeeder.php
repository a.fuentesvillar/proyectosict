<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Promoter;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PromoterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Promoter::truncate();

        $promoter = new Promoter;
        $promoter->cif = "A11223344";
        $promoter->name = "Promotora Falsa 1234, S.A.";
        $promoter->addresse = "calle Falsa 123";
        $promoter->city = "El Puerto de Santa María";
        $promoter->contact = "Pedro Contreras";
        $promoter->phone = "666999000";
        $promoter->mail = "correo@correo.com";
        $promoter->created_at = Carbon::now();
        $promoter->updated_at = Carbon::now();
        $promoter->save();

        $promoter = new Promoter;
        $promoter->cif = "B77000999";
        $promoter->name = "Inmobiliaria Limitada, S.L.";
        $promoter->addresse = "calle de Mentira 99";
        $promoter->city = "San Fernando";
        $promoter->contact = "Javier Merino";
        $promoter->phone = "999666888";
        $promoter->mail = "mail@correo.com";
        $promoter->created_at = Carbon::now();
        $promoter->updated_at = Carbon::now();
        $promoter->save();

        $promoter = new Promoter;
        $promoter->cif = "A33669955";
        $promoter->name = "Astérix y Obelix, S.A.";
        $promoter->addresse = "La Galia";
        $promoter->city = "El Bosque";
        $promoter->contact = "Asterix el Galo";
        $promoter->phone = "900633448";
        $promoter->mail = "mail@correo.com";
        $promoter->created_at = Carbon::now();
        $promoter->updated_at = Carbon::now();
        $promoter->save();

        $promoter = new Promoter;
        $promoter->cif = "B55998877";
        $promoter->name = "Promotora de los Puertos, S.L.";
        $promoter->addresse = "calle Larga";
        $promoter->city = "El Puerto de Santa María";
        $promoter->contact = "El ratón";
        $promoter->phone = "800800800";
        $promoter->mail = "correo@correo.com";
        $promoter->created_at = Carbon::now();
        $promoter->updated_at = Carbon::now();
        $promoter->save();
    }
}
