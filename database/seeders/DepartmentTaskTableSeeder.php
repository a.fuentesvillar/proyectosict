<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\DepartmentTask;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DepartmentTaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        DB::table('department_task')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');

        $dp = new DepartmentTask;
        $dp->department_id = 3;
        $dp->task_id = 5;
        $dp->created_at = Carbon::now();
        $dp->updated_at = Carbon::now();
        $dp->save();

        $dp = new DepartmentTask;
        $dp->department_id = 3;
        $dp->task_id = 6;
        $dp->created_at = Carbon::now();
        $dp->updated_at = Carbon::now();
        $dp->save();

        $dp = new DepartmentTask;
        $dp->department_id = 3;
        $dp->task_id = 7;
        $dp->created_at = Carbon::now();
        $dp->updated_at = Carbon::now();
        $dp->save();

        $dp = new DepartmentTask;
        $dp->department_id = 3;
        $dp->task_id = 8;
        $dp->created_at = Carbon::now();
        $dp->updated_at = Carbon::now();
        $dp->save();

        $dp = new DepartmentTask;
        $dp->department_id = 3;
        $dp->task_id = 9;
        $dp->created_at = Carbon::now();
        $dp->updated_at = Carbon::now();
        $dp->save();

        $dp = new DepartmentTask;
        $dp->department_id = 4;
        $dp->task_id = 1;
        $dp->created_at = Carbon::now();
        $dp->updated_at = Carbon::now();
        $dp->save();

        $dp = new DepartmentTask;
        $dp->department_id = 4;
        $dp->task_id = 5;
        $dp->created_at = Carbon::now();
        $dp->updated_at = Carbon::now();
        $dp->save();

        $dp = new DepartmentTask;
        $dp->department_id = 4;
        $dp->task_id = 6;
        $dp->created_at = Carbon::now();
        $dp->updated_at = Carbon::now();
        $dp->save();

        $dp = new DepartmentTask;
        $dp->department_id = 4;
        $dp->task_id = 7;
        $dp->created_at = Carbon::now();
        $dp->updated_at = Carbon::now();
        $dp->save();

        $dp = new DepartmentTask;
        $dp->department_id = 4;
        $dp->task_id = 8;
        $dp->created_at = Carbon::now();
        $dp->updated_at = Carbon::now();
        $dp->save();

        $dp = new DepartmentTask;
        $dp->department_id = 4;
        $dp->task_id = 9;
        $dp->created_at = Carbon::now();
        $dp->updated_at = Carbon::now();
        $dp->save();

        $dp = new DepartmentTask;
        $dp->department_id = 5;
        $dp->task_id = 2;
        $dp->created_at = Carbon::now();
        $dp->updated_at = Carbon::now();
        $dp->save();

        $dp = new DepartmentTask;
        $dp->department_id = 5;
        $dp->task_id = 3;
        $dp->created_at = Carbon::now();
        $dp->updated_at = Carbon::now();
        $dp->save();

        $dp = new DepartmentTask;
        $dp->department_id = 5;
        $dp->task_id = 4;
        $dp->created_at = Carbon::now();
        $dp->updated_at = Carbon::now();
        $dp->save();

        $dp = new DepartmentTask;
        $dp->department_id = 6;
        $dp->task_id = 10;
        $dp->created_at = Carbon::now();
        $dp->updated_at = Carbon::now();
        $dp->save();
    }
}
