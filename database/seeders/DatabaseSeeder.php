<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(UserTableSeeder::class);
        $this->call(DepartmentTableSeeder::class);
        $this->call(PromoterTableSeeder::class);
        $this->call(TaskTableSeeder::class);
        $this->call(DepartmentTaskTableSeeder::class);
        $this->call(StateTableSeeder::class);
    }
}
