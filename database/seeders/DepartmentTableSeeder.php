<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Department;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Department::truncate();

        $user = new Department;
        $user->name = "Administrador";
        $user->description = "Encargado del mantenimiento del sistema.";
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->save();

        $user = new Department;
        $user->name = "Jefe de Proyectos";
        $user->description = "Organiza y distribuye los proyectos y trabajos.";
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->save();

        $user = new Department;
        $user->name = "Responsable";
        $user->description = "Encargado de un proyecto, sus recursos y su buena ejecución.";
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->save();

        $user = new Department;
        $user->name = "Ingeniero";
        $user->description = "Es la persona que ejecuta los cálculos, memorias, pliegos y mediciones.";
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->save();

        $user = new Department;
        $user->name = "Delineante";
        $user->description = "Elabora y organiza la documentación gráfica y planos.";
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->save();

        $user = new Department;
        $user->name = "Auxiliar";
        $user->description = "Sirve como apoyo al resto de departamentos.";
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->save();
    }
}
