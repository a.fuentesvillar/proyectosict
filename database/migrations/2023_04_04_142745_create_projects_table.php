<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name');
            $table->text('description');
            $table->string('city');
            $table->integer('promoter_id');
            $table->date('orderDate')->nullable();
            $table->integer('user_id');
            $table->integer('state_id');
            // Datos para el predimensionado de la infraestructura
            $table->integer('nPau');
            $table->char('tipoEdif');
            $table->integer('numMaxPauVertical');
            $table->integer('numMaxPauPlanta');
            // Datos Calculados de la infraestrcutura
            $table->string('arqExterior')->nullable();
            $table->string('canalExterior')->nullable();
            $table->string('regEnlace')->nullable();
            $table->string('canalEnlace')->nullable();
            $table->string('recintos')->nullable();
            $table->string('canalPrincipal')->nullable();
            $table->string('regSecundario')->nullable();
            $table->string('canalSecundaria')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('projects');
    }
};
